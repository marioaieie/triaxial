# -*- Makefile -*-
# =======================================================================
#
#				Makefile for TRIAXIAL
#
#       A code to calculate triaxial shape of dark matter haloes 
#
#	By Mario Bonamigo
#
# =======================================================================

# UNSIO := /home/Software/Unsio-1.0.1
 FC := gfortran
 CPPC := h5c++
 CHECK   := #-g -inline_debug_info
 PFLAGS  :=  -fopenmp $(CHECK) -O3 -std=c++11 # -I$(UNSIO)/inc/uns
 LFLAGS  := -lgfortran -fopenmp -lhdf5_hl_cpp -lhdf5_cpp -lhdf5_hl -lhdf5 -lz \
          #-L$(UNSIO)/lib -lunsio -L$(UNSIO)/lib -lnemo -Wl,-rpath=$(UNSIO)/lib


 BINDIR	:= $(HOME)/bin

 SDIR    := ./Sources

 EXEC	:= triaxial_0.99

 F90	:= calc_eigenvalues.f90

 F90O    := $(F90:.f90=.o)

#SRC	:= cospar.f90 make_llist0.f90 make_llist1.f90 prof.f90 \
#	   profiles.f90 read_512.f90 readcomp.f90 read_gadget2.f90

 SRC	:= main.cc halo.cc mxxl.cc
 #mxxl.cc


 OBJ	:= $(SRC:.cc=.o)

 RM	:= rm -f

#
#

default: $(F90O) $(OBJ) Makefile
	$(CPPC) $(F90O) $(OBJ) -o $(BINDIR)/$(EXEC) $(LFLAGS)
#	echo $(HOSTTYPE)


clean:
	$(RM) $(OBJ) $(F90O)

tidy:	clean
	$(RM) $(BINDIR)/$(EXEC)

new:	clean default

%.o: $(SDIR)/%.cc
	$(CPPC) $(PFLAGS) -c $<

%.o: $(SDIR)/%.f90
	$(FC) $(PFLAGS) -c $< 

#%.mod: $(SDIR)/%.f90
#	$(FC) $(FFLAGS) -c $< 




