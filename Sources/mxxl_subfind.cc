#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip> // setprecision(),setfill(),setw()

#include "mxxl.hh"
#include "mxxl_subfind.hh"


int mxxl::load_subhalo( const int fnum )
{

//  Open the file
	std::ostringstream s;
	s << _dir << "Groups_" << std::setw(3) << std::setfill('0') << _snap
	<< "/subhalo_tab_" << std::setw(3) << std::setfill('0') << _snap
	<< "." << fnum;
	std::ifstream fs( s.str().c_str(), std::ios::binary|std::ios::in);
	std::cout << "Opened subfind catalouge:\t" << s.str() << std::endl;

	if ( !fs.is_open() )
	{
		std::cout << "Could not open file " << s.str() << std::endl;
		return 1;
	}


//	reading the header
    int Ngroups, Nids, NTask, Nsubgroups;
    long TotNgroups, TotNids, TotNsubgroups;

    fs.read( (char*)&Ngroups, sizeof(int) );
    fs.read( (char*)&TotNgroups, sizeof(long) );
    fs.read( (char*)&Nids, sizeof(int) );
    fs.read( (char*)&TotNids, sizeof(long) );
    fs.read( (char*)&NTask, sizeof(int) );
    fs.read( (char*)&Nsubgroups, sizeof(int) );
    fs.read( (char*)&TotNsubgroups, sizeof(long) );

    subfind Group(Ngroups, Nsubgroups);

//  #########	Reading Group infos ##############
 
    /* group len */
//	std::vector<int> Len(Ngroups);
//	int *Len = new int[Ngroups];
	fs.read( (char*)&Group.Len[0], Ngroups * sizeof(int) );
	std::cout << Group.Len[0] << "\t" << Group.Len[Ngroups-1] << std::endl;

    /* offset into id-list */
	int *Offset = new int[Ngroups];
	fs.read( (char*)Offset, Ngroups * sizeof(int) );

    /* group number */
	long *GrNr = new long[Ngroups];
	fs.read( (char*)GrNr, Ngroups * sizeof(long) );

    /* CM */
	float *CM = new float[3*Ngroups];
	fs.read( (char*)CM, 3 * Ngroups * sizeof(float) );

    /* vel */
	float *Vel = new float[3*Ngroups];
	fs.read( (char*)Vel, 3 * Ngroups * sizeof(float) );

    /* location (potential minimum) */
	float *Pos = new float[3*Ngroups];
	fs.read( (char*)Pos, 3 * Ngroups * sizeof(float) );

    /* M_Mean200 */
	float *M_Mean200 = new float[Ngroups];
	fs.read( (char*)M_Mean200, Ngroups * sizeof(float) );

    /* M_Crit200 */
	float *M_Crit200 = new float[Ngroups];
	fs.read( (char*)M_Crit200, Ngroups * sizeof(float) );

    /* M_TopHat200 */
	float *M_TopHat200 = new float[Ngroups];
	fs.read( (char*)M_TopHat200, Ngroups * sizeof(float) );
 
    /* FoF Velocity dispersion */
	float *VelDisp = new float[Ngroups];
	fs.read( (char*)VelDisp, Ngroups * sizeof(float) );

    /* VelDisp_Mean200 */
	float *VelDisp_Mean200 = new float[Ngroups];
	fs.read( (char*)VelDisp_Mean200, Ngroups * sizeof(float) );

    /* VelDisp_Crit200 */
	float *VelDisp_Crit200 = new float[Ngroups];
	fs.read( (char*)VelDisp_Crit200, Ngroups * sizeof(float) );

    /* VelDisp_TopHat200 */
	float *VelDisp_TopHat200 = new float[Ngroups];
	fs.read( (char*)VelDisp_TopHat200, Ngroups * sizeof(float) );

    /* number of substructures in FOF group  */
	int *Nsubs = new int[Ngroups];
	fs.read( (char*)Nsubs, Ngroups * sizeof(int) );

    /* first substructure in FOF group  */
	int *FirstSub = new int[Ngroups];
	fs.read( (char*)FirstSub, Ngroups * sizeof(int) );
 


//  #########	Reading SubGroup infos ##############

    /* Len of substructure  */
	int *SubLen = new int[Nsubgroups];
	fs.read( (char*)SubLen, Nsubgroups * sizeof(int) );

    /* offset of substructure  */
	int *SubOffset = new int[Nsubgroups];
	fs.read( (char*)SubOffset, Nsubgroups * sizeof(int) );

    /* GrNr of substructure  */
	long *SubGrNr = new long[Nsubgroups];
	fs.read( (char*)SubGrNr, Nsubgroups * sizeof(long) );

    /* SubNr of substructure  */
	long *SubSubNr = new long[Nsubgroups];
	fs.read( (char*)SubSubNr, Nsubgroups * sizeof(long) );

    /* Pos of substructure  */
	float *SubPos = new float[3*Nsubgroups];
	fs.read( (char*)SubPos, 3 * Nsubgroups * sizeof(float) );

    /* Vel of substructure  */
	float *SubVel = new float[3*Nsubgroups];
	fs.read( (char*)SubVel, 3 * Nsubgroups * sizeof(float) );

    /* Center of mass of substructure  */
	float *SubCM = new float[3*Nsubgroups];
	fs.read( (char*)SubCM, 3 * Nsubgroups * sizeof(float) );

    /* Spin of substructure  */
	float *SubSpin = new float[3*Nsubgroups];
	fs.read( (char*)SubSpin, 3 * Nsubgroups * sizeof(float) );

    /* velocity dispesion  */
	float *SubVelDisp = new float[Nsubgroups];
	fs.read( (char*)SubVelDisp, Nsubgroups * sizeof(float) );

    /* maximum circular velocity  */
	float *SubVmax = new float[Nsubgroups];
	fs.read( (char*)SubVmax, Nsubgroups * sizeof(float) );

    /* radius of maximum circular velocity  */
	float *SubVmaxRad = new float[Nsubgroups];
	fs.read( (char*)SubVmaxRad, Nsubgroups * sizeof(float) );

    /* radius of half the mass  */
	float *SubHalfMass = new float[Nsubgroups];
	fs.read( (char*)SubHalfMass, Nsubgroups * sizeof(float) );

    /* Shape of substructure  */
    float *SubShape = new float[6*Nsubgroups];
    fs.read( (char*)SubShape, 6 * Nsubgroups * sizeof(float) );

    /* Binding Energy  */
	float *SubBinEnergy = new float[Nsubgroups];
	fs.read( (char*)SubBinEnergy, Nsubgroups * sizeof(float) );

    /* Potential Energy  */
	float *SubPotential = new float[Nsubgroups];
	fs.read( (char*)SubPotential, Nsubgroups * sizeof(float) );

    /* Mass at different radii substructure  */
    float *SubProfile = new float[9*Nsubgroups];
    fs.read( (char*)SubProfile, 9 * Nsubgroups * sizeof(float) );


    fs.close();
    
    
    return 0;
    
}
