#include <iostream>
#include <fstream>
#include <iomanip> // setprecision()
#include <limits>
#include <cmath>
#include <sstream>
#include <vector>
#include <omp.h>
#include <cstdlib> // random,atoi
//#include "particle.hh"
#include "halo.hh"
#include "cosmology.hh"
//#include <uns.h>
#include <H5Cpp.h>
#include <hdf5_hl.h>

#include "mxxl.hh"

#include <algorithm>


struct halo_cat
{
    int subfile;
    int subid;
    float m200;
    float n200;
    float ratio;
    float angle;
    
    halo_cat( int subfile, int subid, float m200, float n200, float ratio, float angle ) :
        subfile(subfile), subid(subid), m200(m200), n200(n200), ratio(ratio), angle(angle) {}
};

template<typename T>
std::ostream& binary_write(std::ostream& stream, const T& value){
    return stream.write(reinterpret_cast<const char*>(&value), sizeof(T));
}
void writeonfile(std::ofstream& fileout,halo& thalo)
{				
	float tcm[3] = {thalo.x(),thalo.y(),thalo.z()};
	float eigval[3],eigvec[3][3];
	thalo.eigenvectors(eigvec);
	thalo.eigenvalues(eigval);
	binary_write(fileout,thalo.id());
	binary_write(fileout,thalo.mass());
	binary_write(fileout,thalo.radius());
	binary_write(fileout,tcm);
	binary_write(fileout,eigval);
	binary_write(fileout,eigvec);
//	binary_write(fileout,1);
}


int get_shape(halo &thalo, float ndelta)
{

	float eig_halo[3];
	float s,q,s1,q1;
	thalo.eigenvalues(eig_halo);
	q1 = sqrt(eig_halo[1]/eig_halo[0]);
	s1 = sqrt(eig_halo[2]/eig_halo[0]);

	int it=0;
	do 
	{
		s = s1;
		q = q1;
//		f11 << it << '\t' << s << '\t' << q << std::endl;

		thalo.reshape(ndelta);

		thalo.eigenvalues(eig_halo);
		q1 = sqrt(eig_halo[1]/eig_halo[0]);
		s1 = sqrt(eig_halo[2]/eig_halo[0]);

		it++;
//		if (thalo.mass() < 1000) return -it;

//		} while ( ( (s1-s)*(s1-s)/(s*s) > 0.005 || (q1-q)*(q1-q)/(q*q) > 0.005 ) && it<100);
	} while ( ( std::abs(s1-s)/s > 0.005 || std::abs(q1-q)/q > 0.005 ) && it<100);
//		} while ((std::abs( thalo.rpart(thalo.mass())-rell) /rell) > 0.0001 && it<100);

		return it;
}



int main(int argc, char* argv[])
{

//	const cosmology cosmo(0.27,0.25,100.);

//	
//	std::ofstream fso,feo;
//	std::ostringstream seo,sso;
//	sso << "halo_SO.dat";
//	fso.open(sso.str().c_str(),std::ios::out);
//	seo << "halo_EO.dat";
//	feo.open(seo.str().c_str(),std::ios::out);


//	std::ifstream halocat("/home/data/Music/z_0.250_dishalos",std::ios::in);
////	std::ifstream halocat("/home2/vega/public/z_0.250_dishalos",std::ios::in);
//	char dum[2048];//,tind[3];
//	halocat.getline(dum,2048);
//	int idum, ihalo, tind;
//	tind = 0;
//	float fdum;
//	while (!halocat.eof())
////	for (int i = 0; i < 27; i++)
//	{
//		float cm[3];
//		halocat >> tind >> ihalo >> fdum  >> fdum >> fdum >> cm[0] >> cm[1] >> cm[2];
//		halocat.getline(dum,2048);
//		if (ihalo != 1) continue;

//		std::cout << "Doing halo: "<< tind << "\t\tcm: "<< cm[0] << "\t" << cm[1] << "\t" << cm[2] << "\t" << std::endl;
//		cm[0] *=1000.;
//		cm[1] *=1000.;
//		cm[2] *=1000.;
//		
//		std::stringstream snapfile;
//		snapfile << "/home/data/Music/Snapshots/Cl_" << std::setw(5) << std::setfill('0') << tind << "/snap_010";
////		snapfile << "/data1/users/gustavo/MUSIC/MULTIDARK_ADIABAT/CLUSTER_" << std::setw(5) << std::setfill('0') << tind << "/snap_010";
//		std::cout << "Opening file " << snapfile.str();

//		uns::CunsIn * uns = new uns::CunsIn(snapfile.str(),"all","0.8");
//		std::cout << "\t\tIs format valid: " << uns->isValid();
//		std::cout << "\t\tLoad data: " << uns->snapshot->nextFrame() << std::endl;


//		std::vector<particle> hpart;
//		float px,py,pz;
//		float * pos, *pmasst;
//		float pmass;
//		bool ok;
//		int nbody, npart;

//		npart = 0;
//		std::stringstream species;
//		species << "halo";


//		for (int i = 0; i < 1; i++)
//		{
//			std::cout << "Component: " << species.str();

//			ok = uns->snapshot->getData(species.str(),"pos" ,&nbody,&pos );
//			std::cout << "\t\tRead position of " << nbody << " particles.";
//			ok = uns->snapshot->getData(species.str(),"mass" ,&nbody,&pmasst );
//			pmass = pmasst[0];
//			std::cout << "\t\tParticle mass: " << pmass << std::endl;
//		
//			npart += nbody;
//			for (int jj = 0; jj < nbody; jj++)
//			{
//				px = (pos[3*jj] - cm[0]);
//				py = (pos[3*jj+1] - cm[1]);
//				pz = (pos[3*jj+2] - cm[2]);

//				hpart.push_back(particle(px,py,pz,pmass));
//			}
//				species.str(std::string());
//				species << "gas";
//		}
//	
//	
//		delete uns;
//		
//		halo thalo(tind,npart,300.,cm[0],cm[1],cm[2],hpart);

////		float overdens = 200./cosmo.omegaz*cosmo.rho_back/pow(1000.,3);
//		float overdens = 112.98947423493/0.41937258695274*cosmo.rho_back/pow(1000.,3);

//		thalo.reshape(overdens);
//		float eigval[3],eigvec[3][3];
//		fso << thalo << std::endl;
////		thalo.eigenvectors(eigvec);
//		thalo.eigenvalues(eigval);
//		std::cout << sqrt(eigval[1]/eigval[0]) << "\t" << sqrt(eigval[2]/eigval[0]) << "\t" << thalo.mass() << "\t" << sqrt(thalo.radius()) << std::endl; 
////		std::cout << "eivec = [[" << eigvec[0][0] << ", " << eigvec[1][0] << ", " << eigvec[2][0] << "],"; 
////		std::cout << "[" << eigvec[0][1] << ", " << eigvec[1][1] << ", " << eigvec[2][1] << "],";
////		std::cout << "[" << eigvec[0][2] << ", " << eigvec[1][2] << ", " << eigvec[2][2] << "]]"<< std::endl; 

//		int it = get_shape(thalo,overdens);
//		feo << thalo << "\t" << it << std::endl;		
////		thalo.eigenvectors(eigvec);
//		thalo.eigenvalues(eigval);
//		std::cout << sqrt(eigval[1]/eigval[0]) << "\t" << sqrt(eigval[2]/eigval[0]) << "\t" << thalo.mass() << "\t" << sqrt(thalo.radius()) << "\t" << it << std::endl; 
////		std::cout << "eivec = [[" << eigvec[0][0] << ", " << eigvec[1][0] << ", " << eigvec[2][0] << "],"; 
////		std::cout << "[" << eigvec[0][1] << ", " << eigvec[1][1] << ", " << eigvec[2][1] << "],";
////		std::cout << "[" << eigvec[0][2] << ", " << eigvec[1][2] << ", " << eigvec[2][2] << "]]"<< std::endl; 


//	}







    std::vector<halo_cat> catalouge;
//    halo_struct  p_data[NRECORDS];
//    for ( int i=0; i<NRECORDS; i++ )
//    {
//        p_data[i].id = ids[i];
//        p_data[i].a = ratios[i];        
//    }

//    std::vector<int> ids;
//    std::vector<float> ratios;
//    std::vector<float> angles;

////    int pippo[NX];
//    for( int i=0; i<NX; i++ )
//    {
//        ids[i] = i;
//        std::cout << ids[i] << ", ";
//    }
//    std::cout << std::endl;










	const cosmology cosmo( 0.25, 1., 100. );

	const std::string dir = "/home/mbonamigo/data/Mxxl/";
	const int nfiles = 3072;
	const int snap = 41;
	mxxl sim(dir,snap,nfiles);


	//sim.load_catalouge();
    //	std::vector<float> cmall = sim.cm();
    //	std::vector<float> massall = sim.mass();



    int icat = 0;
    halo thalo;
//    int fnum = 9;
    for ( int fnum = 0; fnum<nfiles; fnum++)
    {
    //  Open the file
	    std::ostringstream s;
	    s << dir << "Groups_" << std::setw(3) << std::setfill('0') << snap
	    << "/subhalo_tab_" << std::setw(3) << std::setfill('0') << snap
	    << "." << fnum;
	    std::ifstream fs( s.str().c_str(), std::ios::binary|std::ios::in);
	    std::cout << "\tOpened subfind catalouge:\t" << s.str() << std::endl;

	    if ( !fs.is_open() )
	    {
		    std::cout << "Could not open file " << s.str() << std::endl;
		    return 1;
	    }


    //	reading the header
        int Ngroups, Nids, NTask, Nsubgroups;
        long TotNgroups, TotNids, TotNsubgroups;

        fs.read( (char*)&Ngroups, sizeof(int) );
        fs.read( (char*)&TotNgroups, sizeof(long) );
        fs.read( (char*)&Nids, sizeof(int) );
        fs.read( (char*)&TotNids, sizeof(long) );
        fs.read( (char*)&NTask, sizeof(int) );
        fs.read( (char*)&Nsubgroups, sizeof(int) );
        fs.read( (char*)&TotNsubgroups, sizeof(long) );

	    int *idum = new int;
	    long *ldum = new long;
	    float *fdum = new float;
	    float *M_Crit200 = new float[Ngroups];
	    int *Nsubs = new int[Ngroups];
	    int *FirstSub = new int[Ngroups];
	    int *Np_Sub200c = new int[Nsubgroups];
	    float *SubPos = new float[3*Nsubgroups];


    //  #########	Reading Group infos ##############
     
    //	std::vector<int> Len(Ngroups);
    //	fs.read( (char*)&Len[0], Ngroups * sizeof(int) );
	    idum = new int[Ngroups];
        ldum = new long[Ngroups];
	    fdum = new float[3*Ngroups];
	    fs.read( (char*)idum, Ngroups * sizeof(int) );    /* group len */
	    fs.read( (char*)idum, Ngroups * sizeof(int) );    /* offset into id-list */
	    fs.read( (char*)ldum, Ngroups * sizeof(long) );    /* group number */
	    fs.read( (char*)fdum, 3 * Ngroups * sizeof(float) );    /* CM */
	    fs.read( (char*)fdum, 3 * Ngroups * sizeof(float) );    /* vel */
	    fs.read( (char*)fdum, 3 * Ngroups * sizeof(float) );    /* location (potential minimum) */
        delete idum;
        delete ldum;
        delete fdum;


	    fdum = new float[Ngroups];
	    fs.read( (char*)fdum, Ngroups * sizeof(float) );    /* M_Mean200 */
	    fs.read( (char*)M_Crit200, Ngroups * sizeof(float) );    /* M_Crit200 */
	    fs.read( (char*)fdum, Ngroups * sizeof(float) );    /* M_TopHat200 */
	    fs.read( (char*)fdum, Ngroups * sizeof(float) );    /* FoF Velocity dispersion */
	    fs.read( (char*)fdum, Ngroups * sizeof(float) );    /* VelDisp_Mean200 */
	    fs.read( (char*)fdum, Ngroups * sizeof(float) );    /* VelDisp_Crit200 */
	    fs.read( (char*)fdum, Ngroups * sizeof(float) );    /* VelDisp_TopHat200 */
        delete fdum;


	    fs.read( (char*)Nsubs, Ngroups * sizeof(int) );    /* number of substructures in FOF group  */
	    fs.read( (char*)FirstSub, Ngroups * sizeof(int) );    /* first substructure in FOF group  */
     

	    idum = new int[Nsubgroups];
	    ldum = new long[Nsubgroups];
	    fs.read( (char*)Np_Sub200c, Nsubgroups * sizeof(int) );    /* Len of substructure  */
	    fs.read( (char*)idum, Nsubgroups * sizeof(int) );    /* Offset of substructure  */
	    fs.read( (char*)ldum, Nsubgroups * sizeof(long) );    /* GrNr of substructure  */
	    fs.read( (char*)ldum, Nsubgroups * sizeof(long) );    /* Subgroup number of substructure  */
        delete idum;
        delete ldum;

        /* Pos of substructure  */
	    fs.read( (char*)SubPos, 3 * Nsubgroups * sizeof(float) );

        fs.close();


    //    std::vector<int> (Ngroups);   
    //subs = s.spos[ s.Fsub[i] : s.Fsub[i] + s.Nsubs[i] ] - s.pos[i]
    //smask = np.sum( subs[:,:-1]**2 , axis=-1) < r200c[i]**2

//        ids.resize( ids.size()+Ngroups );
//        ratios.resize( ratios.size()+Ngroups );
//        angles.resize( angles.size()+Ngroups );

        for ( int subid = 0; subid < Ngroups; subid++)
        {
	        std::vector<particle> hpart;
	        float px,py,pz;
	        float pmass = sim.pmass();
	        
	        float n200 = 0.;
	        float ratio = 0.;
	        float angle = 0.;
	        int mass = 0.;

            // Halo 'subid' is in MXXL catalouge
            if ( M_Crit200[subid] > 1e2 )
            {
                
//                // Check if this is the correct halo in the catalouge
//                if ( M_Crit200[subid] != massall[icat] )
//                {
//                    std::cout << "Something is not right... Halo " << subid <<
//                    " of file " << fnum << " is not " << icat << std::endl;
//                    return 1;
//                }
                
                float r200c = pow( M_Crit200[subid] * cosmo.omegaz /200. /cosmo.rho_back / (4.*M_PI/3), 1./3.);
	            float r2 = r200c*r200c;

	            for (int jj = FirstSub[subid]; jj < FirstSub[subid] + Nsubs[subid]; jj++)
	            {
		            px = SubPos[3*jj+0] - SubPos[3*FirstSub[subid]+0];
		            py = SubPos[3*jj+1] - SubPos[3*FirstSub[subid]+1];
		            pz = SubPos[3*jj+2] - SubPos[3*FirstSub[subid]+2];

                    if ( px*px + py*py <= r2 )
                    {
		      //		      Np_Sub200c[jj] = 1;
                        hpart.emplace_back( px, py, 0., Np_Sub200c[jj] );
                        mass += Np_Sub200c[jj];
//                        hpart.emplace_back( px,py,0.,pmass );
                    }
	            }
	
	            if (hpart.size() > 10)
                {
	                thalo = halo( icat, hpart.size(), r200c, 0., 0., 0., hpart );

                    thalo.compute_mtensor();
                    thalo.mtensor();

	                float eigval[3],eigvec[3][3];
	                thalo.eigenvectors(eigvec);
	                thalo.eigenvalues(eigval);
	                
	                ratio = sqrt( eigval[1]/eigval[0] );
	                angle = acos( eigvec[0][0] );
	                
                    
//                    ids.push_back( thalo.id() );
//                    ratios.push_back( sqrt( eigval[1]/eigval[0] ) );
//                    angles.push_back( acos( eigvec[0][0] )  );

    //	std::cout << count << "\t" << sqrt(eigval[1]/eigval[0]) << "\t" << sqrt(eigval[2]/eigval[0]) << std::endl; 
    //	std::cout << "eivec = [[" << eigvec[0][0] << ", " << eigvec[1][0] << ", " << eigvec[2][0] << "],"; 
    //	std::cout << "[" << eigvec[0][1] << ", " << eigvec[1][1] << ", " << eigvec[2][1] << "],";
    //	std::cout << "[" << eigvec[0][2] << ", " << eigvec[1][2] << ", " << eigvec[2][2] << "]]"<< std::endl; 
    //                return 0;

                }
                
                catalouge.emplace_back( fnum, subid, M_Crit200[subid], hpart.size(), ratio, angle );
                icat++;
            }
        }
        
        delete M_Crit200;
	    delete Nsubs;
	    delete FirstSub;
	    delete SubPos;
	    delete Np_Sub200c;
    }




    const hsize_t NFIELDS = 6;
    const hsize_t NRECORDS = catalouge.size();
    
    /* Calculate the size and the offsets of our struct members in memory */
    size_t dst_size =  sizeof( halo_cat );
    size_t dst_offset[NFIELDS] = { HOFFSET( halo_cat, subfile ),
                                   HOFFSET( halo_cat, subid ),
                                   HOFFSET( halo_cat, m200 ),
                                   HOFFSET( halo_cat, n200 ),
                                   HOFFSET( halo_cat, ratio ),
                                   HOFFSET( halo_cat, angle ),
                                   };



    /* Define field information */
    const char *field_names[NFIELDS]  =
    { "subfile", "subid", "m200", "n200", "ratio", "angle" };
    hid_t      field_type[NFIELDS];
    hid_t      file_id;
    hsize_t    chunk_size = 1000;
    int        *fill_data = NULL;
    int        compress  = 0;
    int        i;


    /* Initialize field_type */
    field_type[0] = H5T_NATIVE_INT;
    field_type[1] = H5T_NATIVE_INT;
    field_type[2] = H5T_NATIVE_FLOAT;
    field_type[3] = H5T_NATIVE_FLOAT;
    field_type[4] = H5T_NATIVE_FLOAT;
    field_type[5] = H5T_NATIVE_FLOAT;


    /* Create a new file using default properties. */
    file_id = H5Fcreate( "subcat_weighted.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

    /*-------------------------------------------------------------------------
    * H5TBmake_table
    *-------------------------------------------------------------------------
    */
    

    std::cout << "Making table" << std::endl;
    H5TBmake_table( "MXXL", file_id, "MXXL", NFIELDS, NRECORDS,
                     dst_size,field_names, dst_offset, field_type,
                     chunk_size, fill_data, compress, &catalouge[0]  );
    std::cout << "Done table" << std::endl;


    /* close the file */
    H5Fclose( file_id );




//	std::ofstream fileout;
//	fileout.open( "pippo.dat", std::ios::out | std::ios::binary );

//    size_t size = ids.size();

//    fileout.write((char*)&size, sizeof(size));
//    fileout.write((char*)&ids[0], size * sizeof(int));
//    fileout.write((char*)&ratios[0], size * sizeof(float));
//    fileout.write((char*)&angles[0], size * sizeof(float));

//    fileout.close();

	return 0;
}	

	   
//	int box,basex,basey,basez,buffer;
//	std::string fileid;
//	buffer = 1;
//	box = 10;
//	basex = basey = basez = 0;
//	switch (argc-1 )
//	{
//	case 0:
//		break;
//	case 2:
////		Quite useless, but for testing...
//		box = std::atoi(argv[1]);
//		basex = basey = basez = std::atoi(argv[2]);
//		break;
//    case 5:
//        fileid = argv[5];
//	case 4:
//		box = std::atoi(argv[1]);
//		basex = std::atoi(argv[2]);
//		basey = std::atoi(argv[3]);
//		basez = std::atoi(argv[4]);
//		break;
//	default:
//		std::cout << "Incorrect number of arguments (" << argc << "). Needed 0,2 or 4" << std::endl;
//		return 0;
//	}

////	const std::string dir = "/data/mbonamigo/Mxxl/";
//	const std::string dir = "/home/data/Mxxl/";
//	const int nfiles = 3072;
//	const int snap = 63;
//	mxxl sim(dir,snap,nfiles);

//	sim.load_catalouge();
//	
//	std::vector<float> cmall = sim.cm();
//	std::vector<float> massall = sim.mass();


//	float x0,x1,y0,y1,z0,z1;
//	x0 = basex *sim.boxsize() / sim.hashtablesize();
//	x1 = (basex+box) *sim.boxsize() / sim.hashtablesize();
//	y0 = basey *sim.boxsize() / sim.hashtablesize();
//	y1 = (basey+box) *sim.boxsize() / sim.hashtablesize();
//	z0 = basez *sim.boxsize() / sim.hashtablesize();
//	z1 = (basez+box) *sim.boxsize() / sim.hashtablesize();
//	
//	box += 2*buffer;
//	basex -= buffer;
//	basey -= buffer;
//	basez -= buffer;
//	int minibox[] = {basex,basex+box,basey,basey+box,basez,basez+box};
////	int minibox[] = {basex-buffer,basex+box+buffer,basey-buffer,basey+box+buffer,basez-buffer,basez+box+buffer};
//	sim.load_files(minibox);


////	std::ofstream fso,feo;
////	std::ostringstream seo,sso;
////	sso << "halo_SO_" << std::setw(3) << std::setfill('0') << snap << "_" << fileid << ".dat";
////	fso.open(sso.str().c_str(),std::ios::out);
////	seo << "halo_EO_" << std::setw(3) << std::setfill('0') << snap << "_" << fileid << ".dat";
////	feo.open(seo.str().c_str(),std::ios::out);


////	std::ostringstream ssel;
////	ssel << "random_haloes_" << std::setw(3) << std::setfill('0') << snap << ".txt";
////	std::ifstream randomh(ssel.str().c_str(),std::ios::in);
//	int tind,ntot;
////	char dum[256];
////	ntot = 0;
////	while (randomh.getline(dum,256)) ntot++;
////	std::cout << "Found "<< ntot << " haloes" << std::endl;
////	randomh.clear();
////	randomh.seekg (0, randomh.beg);
//	
//	ntot = sim.nhalo();
////	std::cout << "Found "<< ntot << " haloes" << std::endl;	
////	int _todo[ntot];
////	for (int i = 0; i < ntot; i++)
////	{
//////		randomh >> _todo[i];
////		_todo[i] = i;
////	}

//	const float rscale = 3. * 1.8*cbrt(sim.cosmo.G*sim.cosmo.omegaz / sim.cosmo.Omega0) /100.;


////	for (int id = 0; id < sim.nhalo(); id++)
//	for (int i = 0; i < ntot; i++)
//	{
//		int id = i;//_todo[i];
//		float cm[3];
//		cm[0] = cmall[3*id+0];
//		cm[1] = cmall[3*id+1];
//		cm[2] = cmall[3*id+2];
//		float radius = rscale*cbrt(massall[id]);
////		std::cout << cm[0] << "\t" << cm[1] << "\t" << cm[2] << "\t" << massall[id]/sim.pmass()<< std::endl;

//		if (cm[0] <= x1 && cm[0] > x0 && cm[1] <= y1 && cm[1] > y0 && cm[2] <= z1 && cm[2] > z0 )
//		{
//			halo thalo = sim.get_halo(id,cm,radius);
//	//		thalo.reshape(sim.cosmo.delta_cri * sim.cosmo.rho_back/sim.pmass());
//			thalo.reshape(89.934955076540/0.25004925418253*sim.cosmo.rho_back/sim.pmass());
//			return 0;
////			fso << thalo << std::endl;
//			int it = get_shape(thalo,89.934955076540/0.25004925418253*sim.cosmo.rho_back/sim.pmass());
////			feo << thalo << "\t" << it << std::endl;		
//	//		std::cout << thalo.mass()-massall[id]/sim.pmass() << std::endl;
//		}
//	}
//	
////	{
////		halo thalo = sim.get_halo(id,cm,radius);
////		thalo.reshape(sim.cosmo.delta_cri * sim.cosmo.rho_back/sim.pmass());
////		std::cout << thalo << std::endl;
////	}

//	
//////					 COMPUTE SHAPES
////		float dens[10];
////		for (int nn = 0; nn < 7; nn++) dens[nn] = pow(2.,nn);
////		dens[7] = -200./cosmo::omegaz;
////		dens[8] = -500./cosmo::omegaz;
////		dens[9] = -2500./cosmo::omegaz;

////		for (int nn = 0; nn < 10; nn++){
////			halo thalo(_todo[i],ii,radius,cm[0],cm[1],cm[2],hpart);
////			thalo.reshape(dens[nn]);
////			writeonfile(fso[omp_get_thread_num()],thalo);
////			binary_write(fso[omp_get_thread_num()],0);
////			int it = 0;
////			if (thalo.mass() >= 1000) it = get_shape(thalo,dens[nn]);
////			writeonfile(feo[omp_get_thread_num()],thalo);
////			binary_write(feo[omp_get_thread_num()],it);
////		}

////	}	
////	std::cout << "Box : " << box << "\tTransfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;


////	fso.close();
////	feo.close();


















////	float* part = sim.get_part();

////	int pippo = 0;
////	int npart = sim.nparticles();
////	int N=10;
////	int idmax = ceil(npart/float(N));
////	int *id;
////	float *pos;
////	std::cout << idmax << "\t" << 3*idmax << "\t" << npart-(N-1)*idmax << std::endl;
////	for(int i=0; i<N; i++)
////	{
////		int jmax = idmax;
////		if (i==N-1) jmax = npart-(N-1)*idmax;
////		id = new int[idmax];
////		pos = new float[3*idmax];
////		if (jmax>0)
////		{
////			for(int j=0; j<jmax; j++)
////			{
////				int idj = i*idmax + j;
////				id[j] = idj;
//////				std::cout << id[j] << "\t" << 3*j+0 << " " << 3*j+1 << " " << 3*j+2 << "\t" << 3*id[j]+0 << " " << 3*id[j]+1 << " " << 3*id[j]+2 << std::endl;
////				pos[3*j+0] = part[3*idj+0];
////				pos[3*j+1] = part[3*idj+1];
////				pos[3*j+2] = part[3*idj+2];
////			}
////			std::cout << "----------------" << std::endl;
////			std::ostringstream s2;
////			s2 << "miniXXL." << i;
////			uns::CunsOut * uns = new uns::CunsOut(s2.str(),"gadget2",0);
////			bool ok;
////			ok = uns->snapshot->setData("dm","id",jmax,id,true);
////			ok = uns->snapshot->setData("dm","pos",jmax,pos,true);
////			std::cout << "data "<< i << ": " << ok << std::endl;
////			ok = uns->snapshot->save();
////			std::cout << "Saved "<< i << ": " << ok << std::endl;
////			pippo += jmax;
////		}
////	}
////	std::cout<<pippo << std::endl;


	

////////////////////////////////////////////////////////////////////////////////////////////////	
	

//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (_inside[k])
//		{
//			// Do your stuff...
////			std::cout << "I'm doing my stuff... " << _nhaloes[k] << std::endl;
////		if (load_head(k)) return 1;

//			for (int i = 0; i < _nhaloes[k]; i++)
//			{

//				if (_todo[haloindex[k]+i])
//				{
//					if (_cm[k][3*i] <= x1 && _cm[k][3*i] > x0 && _cm[k][3*i+1] <= y1 && _cm[k][3*i+1] > y0 && _cm[k][3*i+2] <= z1 && _cm[k][3*i+2] > z0 )
//					{
//						float radius = 3 *cbrt(2*43.0071*_m200[k][i] / (200*0.25*10000));
////						if (_id[haloindex[k]+i] == -1)
////						{
////							std::cout << "mmm... sth smells here... " << haloindex[k]+i << std::endl;
////							return 1;
////						}
//	//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
//					
//						int i0,i1,j0,j1,k0,k1;
//						i0 = floor( (_cm[k][3*i]-radius) / 3000. * HashTabSize );
//						i1 = floor( (_cm[k][3*i]+radius) / 3000. * HashTabSize );
//						j0 = floor( (_cm[k][3*i+1]-radius) / 3000. * HashTabSize );
//						j1 = floor( (_cm[k][3*i+1]+radius) / 3000. * HashTabSize );
//						k0 = floor( (_cm[k][3*i+2]-radius) / 3000. * HashTabSize );
//						k1 = floor( (_cm[k][3*i+2]+radius) / 3000. * HashTabSize );
//	
//	//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
//						if (load_files(needed,i0,i1+1,j0,j1+1,k0,k1+1)) return 1;
//					}
//				}
//			}
//		}
//	}

////	std::cout << "Inside: \t";
////	for (int k = 0; k < nfiles; k++) if (_inside[k]) std::cout << k << "\t";
////	std::cout << std::endl;

////	std::cout << "Needed: \t";
//	ram = 0;
//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (needed[k])
//		{
////			std::cout << k << "\t";
//			ram++;
//		}
//	}
////	std::cout << std::endl;
//	if (ram>30)
//	{
//		std::cout << "Too much RAM!!!";
//		return 1;
//	}

////	std::cout << "Partlist: \t";
////	for (int k = 0; k < nfiles; k++) if (_partlist[k]) std::cout << k << "\t";
////	std::cout << std::endl;

//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (_partlist[k] && !needed[k] && !_inside[k])
//		{
////			std::cout << "Unloading file " << k << std::endl;
////			delete _partlist[k];
//			_partlist[k] = 0;
//		}
//	}
//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (!_partlist[k] && needed[k])			
//		{
//			// Load files
////			std::cout << "Loading file " << k << std::endl;
//			count++;
////			if (load_snap(k)) return 1;
////			_partlist[k] = 1;
//		}
//	}

////	std::cout << "Loading, DONE!" << std::endl;

//	for (int k = 0; k < nfiles; k++) 
//	{
////		if (files[l]) std::cout << l*files[l] << "\t";
//		if (_inside[k])
//		{
//			// Do your stuff...
////			#pragma omp parallel for default(shared) reduction(+:numhalo)
//			for (int i = 0; i < _nhaloes[k]; i++)
//			{
//				if (_todo[haloindex[k]+i])
//				{
//					float cm[3];
//					cm[0] = _cm[k][3*i+0];
//					cm[1] = _cm[k][3*i+1];
//					cm[2] = _cm[k][3*i+2];
//				
//					if (cm[0] <= x1 && cm[0] > x0 && cm[1] <= y1 && cm[1] > y0 && cm[2] <= z1 && cm[2] > z0 )
//					{
//				
//							numhalo++;
//							std::cout << haloindex[k]+i << std::endl;
//						
//						
//	//		\/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/

////						bool selected[nfiles];
////						for (int l = 0; l < nfiles; l++) selected[l] = 0;
////			
////						float radius = 3 *cbrt(2*43.0071*_m200[k][i] / (200*0.25*10000));
////	//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
////						float cube[6];//hx0,hx1,hy0,hy1,hz0,hz1;
////						cube[0] = cm[0]-radius;
////						cube[1] = cm[0]+radius;
////						cube[2] = cm[1]-radius;
////						cube[3] = cm[1]+radius;
////						cube[4] = cm[2]-radius;
////						cube[5] = cm[2]+radius;
////						int i0,i1,j0,j1,k0,k1;
////						i0 = floor( cube[0] / 3000. * HashTabSize );
////						i1 = floor( cube[1] / 3000. * HashTabSize );
////						j0 = floor( cube[2] / 3000. * HashTabSize );
////						j1 = floor( cube[3] / 3000. * HashTabSize );
////						k0 = floor( cube[4] / 3000. * HashTabSize );
////						k1 = floor( cube[5] / 3000. * HashTabSize );
////	//					std::cout << "\tcm: " << k << "\t" << i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << "\t" << radius << std::endl;
////	//					std::cout << "\tcube: " << cube[0] << "\t" << cube[1] << "\t" << cube[2] << "\t" << cube[3] << "\t" << cube[4] << "\t" << cube[5] << std::endl;
////	//					std::cout << "\tcube: " << cube[0]/ 3000. * HashTabSize << "\t" << cube[1]/ 3000. * HashTabSize << "\t" << cube[2]/ 3000. * HashTabSize << "\t" << cube[3]/ 3000. * HashTabSize << "\t" << cube[4] / 3000. * HashTabSize<< "\t" << cube[5] / 3000. * HashTabSize << std::endl;
////	//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
////	//					std::cout << "\thash: " << (-i0)%HashTabSize << "\t" << i0%HashTabSize << "\t" << (-j0)%HashTabSize << "\t" << j0%HashTabSize << "\t" << (-k0)%HashTabSize << "\t" << k0%HashTabSize  << std::endl;
////	
////	//					std::cout << "File " << k << "\tHalo " << i << "\tRadius "<< radius << std::endl;
////						if (load_files(selected,i0,i1+1,j0,j1+1,k0,k1+1));// return 1;
////	//	std::cout << "selected: \t";
////	//	for (int l = 0; l < nfiles; l++) if (selected[l]) std::cout << l << "\t";
////	//	std::cout << std::endl;
////	//						return 0;


////						std::vector<particle> hpart;
////						hpart.resize(0);
////						int ii = 0;

////	//					int l = k;
////						for (int l = 0; l < nfiles; l++)
////						{
////							if (selected[l])
////							{

////								float px,py,pz;
////								hpart.reserve(hpart.size()+(int)(_nparticles[l]/10));
////								if (cube[0] > 0 && cube[1] < 3000 && cube[2] > 0 && cube[3] < 3000 && cube[4] > 0  && cube[5] < 3000) 
////								{
////	//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, cube);
////									read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, cube);
////	 							} else {
////	//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, radius);
////									read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, radius);								
////								}

////								hpart.resize(ii);
////							}

////						}

////	//					 COMPUTE SHAPES
////						halo thalo(haloindex[k]+i,ii,radius,cm[0],cm[1],cm[2],hpart);
////						thalo.reshape();
////						fso << thalo << std::endl;
////						int it = get_shape(thalo);
////						feo << thalo << '\t' << it << std::endl;

////	//						fso << haloindex[k]+i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << std::endl;



////	//					COMPUTE DENSITY PROFILES
////	//					float rcri = cbrt( _mcri[k][i] / cosmo::delta_c/ cosmo::rho_back / (4.*M_PI/3.)  );
////	//					halo thalo(haloindex[k]+i,ii,rcri*rcri,cm[0],cm[1],cm[2],hpart);

////	//{
////	//					halo thalo(haloindex[k]+i,ii,_rvir[_id[haloindex[k]+i]],cm[0],cm[1],cm[2],hpart);


////	//	float rcri = thalo.rcri();
////	//	fso << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << std::endl;
////	//	float density[nbin];
////	//	float rmean[nbin];
////	//	for (int jj = 0; jj < nbin; jj++) 
////	//	{
////	//		density[jj] = 0;
////	//		rmean[jj] = 0;
////	//	}
////	//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
////	//	if ( pippo ) 
////	//	{
////	//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
////	//		return 1;
////	//	}
////	//	

////	//	int idpippo = thalo.id();
////	//	fprofs.write((char*)&idpippo,4);
////	//	fprofs.write((char*)rmean,4*nbin);
////	//	fprofs.write((char*)density,4*nbin);
////	//}



////	//					int ih = _id[haloindex[k]+i];
////	//					halo thalo(haloindex[k]+i,ii,_rvir[_id[haloindex[k]+i]],cm[0],cm[1],cm[2],_axis[ih],_evec[ih],hpart);

////	////					get_conc(thalo);

////	//	float rcri = thalo.rcri();
////	//	feo << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << std::endl;
////	//	float density[nbin];
////	//	float rmean[nbin];
////	//	for (int jj = 0; jj < nbin; jj++) 
////	//	{
////	//		density[jj] = 0;
////	//		rmean[jj] = 0;
////	//	}
////	//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
////	//	if ( pippo ) 
////	//	{
////	//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
////	//		return 1;
////	//	}
////	//	int idpippo = thalo.id();
////	//	fprof.write((char*)&idpippo,4);
////	//	fprof.write((char*)rmean,4*nbin);
////	//	fprof.write((char*)density,4*nbin);




//					}
//				}
//			}

//		}
//	}
//	if (ram > maxram) maxram = ram;
////	std::cout << "Transfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;

//			}	
//		}
////	return 0;
//	}	
////	std::cout << "Box : " << box << "\tTransfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;


////	fso.close();
////	feo.close();
////	fprofs.close();
////	fprof.close();

//	return 0;
//}
