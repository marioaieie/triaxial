#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip> // setprecision(),setfill(),setw()
#include <uns.h>
#include "inc/uns/uns.h"

#include "music.hh"

struct struct_cell
{
   int Start;
   int Last;
} *Cell;

struct struct_list
{
  int FileNr;
  int next;
} *List;


music::music(const std::string dir, const int snap, const int nfiles) : _dir(dir), _snap(snap), _nfiles(nfiles), cosmo(0.25), simulation(dir,snap,nfiles)//, _partlist(0)
{

	uns::CunsIn * uns = new uns::CunsIn(simname,select_c,select_t,verbose);


	load_hash();
	
//	read particles file
	std::ostringstream s2;
	s2 << _dir << "/Fof/halo_part.0";
	std::ifstream fp(s2.str().c_str(),std::ios::binary|std::ios::in);

	//	reading header
	int idumb;
	double time;
	fp.read((char*)&idumb,4);
	fp.read((char*)&_pmass,8);
	fp.read((char*)&time,8);
	fp.read((char*)&_boxsize,8);

}



int music::cell(int k) const
{
	return _List[_Cell[k].Start].FileNr;
}

int music::load_catalouge()
{
	std::ostringstream s;
	s << _dir << "Fof/vhalos_" << std::setw(3) << std::setfill('0') << _snap << "_1e12.dat";
	std::ifstream fc(s.str().c_str(),std::ios::binary|std::ios::in);

	if (fc.is_open())
	{
		fc.read((char*)&_nhalo,4);
		std::cout << "nhalo:" << "\t" << _nhalo << std::endl;

		float vmax;
		_cm.resize(3*_nhalo);
		_mass.resize(_nhalo);
		for (int i = 0; i < _nhalo; i++)
		{
			fc.read((char*)&_cm[3*i],12);
			fc.read((char*)&_mass[i],4);
			fc.read((char*)&vmax,4);
		}
		fc.close();
	
	} else {
		std::cout << "Could not open file " << s.str() << std::endl;
		return 1;
	}

	std::cout << "Catalouge read correctly." << std::endl;
	return 0;
	
}

int music::select_files(bool *files, int xb[6])
{
	for (int ix = xb[0]; ix < xb[1]; ix++)
	{
		for (int iy = xb[2]; iy < xb[3]; iy++)
		{
			for (int iz = xb[4]; iz < xb[5]; iz++)
			{
			int h = _Cell[( ((ix+256)%_HashTabSize) * _HashTabSize + ((iy+256)%_HashTabSize) ) * _HashTabSize + ((iz+256)%_HashTabSize)].Start;
				for (h; h != -1; h = _List[h].next)
				{
				// File to load	
					files[_List[h].FileNr] = 1;
				}
			}	
		}
	}
	
	return 0;
}

int music::load_files(int xb[6])
{
	bool _inside[_nfiles];
	for (int k = 0; k < _nfiles; k++) _inside[k] = 0;

	select_files(_inside,xb);

	int nsel = 0;
	float cm[3];
	float ram = 0;

	_part.reserve(3*1.1*(xb[1]-xb[0])*(xb[3]-xb[2])*(xb[5]-xb[4])*pow(6720/float(_HashTabSize),3.) );


////////////////////    DEBUG   /////////////////////////
//	std::cout << "_part.size(): " << _part.size() << "\t_part.capacity(): " << _part.capacity() << std::endl;

	float cube[6];
	std::cout << "Inside: ";
	for(int i=0; i<6; i++)
	{
		cube[i] = xb[i]/float(_HashTabSize)*_boxsize;
		std::cout << cube[i] << "\t";
	}
	std::cout << std::endl;
//	std::cout << "Box size: " << (xb[1]-xb[0])/float(_HashTabSize)*3000. << std::endl;
//	std::cout << "Expected size: "<< (xb[1]-xb[0])*(xb[3]-xb[2])*(xb[5]-xb[4])/float(pow(_HashTabSize,3))*_nfiles*1.11 << " GB and particles: " << (xb[1]-xb[0])*(xb[3]-xb[2])*(xb[5]-xb[4])*pow(6720/float(_HashTabSize),3.) << std::endl;
///////////////////////////////////////////////////////////



	for (int k = 0; k < _nfiles; k++){
		if (_inside[k])
		{
			float *partlist = load_snap(k);
//			select_part(_part, partlist, 0, , cube);

			if (cube[0] > 0 && cube[1] < 3000 && cube[2] > 0 && cube[3] < 3000 && cube[4] > 0  && cube[5] < 3000) 
			{

				for (int jj = 0; jj < _nparticles; jj++)
				{

					if (partlist[3*jj] > cube[0] && partlist[3*jj] < cube[1] && partlist[3*jj+1] > cube[2] && partlist[3*jj+1] < cube[3] && partlist[3*jj+2] > cube[4] && partlist[3*jj+2] < cube[5] )
					{
						_part.push_back(partlist[3*jj]);
						_part.push_back(partlist[3*jj+1]);
						_part.push_back(partlist[3*jj+2]);
					}

				}
			} else {
				float px,py,pz;
				for (int jj = 0; jj < _nparticles; jj++)
				{
					px = partlist[3*jj];
					py = partlist[3*jj+1];
					pz = partlist[3*jj+2];
					if ( cube[0] < 0. && px >= 1500.) px = px - 3000.;
					if ( cube[2] < 0. && py >= 1500.) py = py - 3000.;
					if ( cube[4] < 0. && pz >= 1500.) pz = pz - 3000.;
					if ( cube[1] >= 3000. && px < 1500.) px = px + 3000.;
					if ( cube[3] >= 3000. && py < 1500.) py = py + 3000.;
					if ( cube[5] >= 3000. && pz < 1500.) pz = pz + 3000.;
					if (px >= cube[0] && px < cube[1] && py >= cube[2] && py < cube[3] && pz >= cube[4] && pz < cube[5] )
					{
						_part.push_back(px);
						_part.push_back(py);
						_part.push_back(pz);
					}

				}
			}


			delete [] partlist;
			partlist = 0;
		}
	}


////////////////////    DEBUG   /////////////////////////
//	std::cout << "Reserved size: "<< 1.1*3*4*(xb[1]-xb[0])*(xb[3]-xb[2])*(xb[5]-xb[4])*pow(6720/float(_HashTabSize),3.)/1024./1024./1024. << " GB and particles: " << 1.1*(xb[1]-xb[0])*(xb[3]-xb[2])*(xb[5]-xb[4])*pow(6720/float(_HashTabSize),3.) << std::endl;
//	std::cout << sizeof(particle) << " RAM (particles):"<< (sizeof(particle)*_part.size()/3)/1024./1024./1024. << " GB\t" << std::endl;
//	ram = sizeof(float)*_part.size();
//	std::cout << " RAM(floats):\t"<< ram/1024./1024./1024. << " GB\t" << _part.size()/3 << std::endl;
//	std::cout << "Used RAM(all):\t"<< (291787512+ram)/1024./1024./1024. << " GB" << std::endl;
//	std::cout << "File size:\t"<< ram/3.*4./1024./1024./1024. << " GB" << std::endl;
///////////////////////////////////////////////////////////////	


	return 0;
}


float* music::load_snap(int k)
{
//	float *partlist;
	//	read particles file
	std::ostringstream s2;
	s2 << _dir << "Snapshot_" << std::setw(3) << std::setfill('0') << _snap << "/snap_music." << k;
	std::ifstream fp(s2.str().c_str(),std::ios::binary|std::ios::in);
	if (fp.is_open())
	{
		std::cout << "Opened file:\t" << s2.str() << std::endl;

		//	reading header
		int nread;
		fp.read((char*)&nread,4);
		_nparticles = nread;

		float *partlist = new float[3*nread];
		fp.read((char*)partlist,12*nread);
		fp.close();

		std::cout << "Particles read! " << std::endl;

		return partlist;

	} else {
		std::cout << "Could not open file " << s2.str() << std::endl;
		return 0;
	}

}

int music::select_part(std::vector<particle> &part, float *partlist, int jjstart, int jjend, float cube[6])
{
	for (int jj = jjstart; jj < jjend; jj++)
	{
		if (partlist[3*jj] > cube[0] && partlist[3*jj] < cube[1] && partlist[3*jj+1] > cube[2] && partlist[3*jj+1] < cube[3] && partlist[3*jj+2] > cube[4] && partlist[3*jj+2] < cube[5] )
		{
			part.push_back(particle(partlist[3*jj],partlist[3*jj+1],partlist[3*jj+2]));
		}
	}
}


int music::select_part(std::vector<particle> &hpart, int &ii, const float cm[3], const float cube[6])
{
	float px,py,pz;
//	std::cout << hpart[0].x() << std::endl;
//	std::cout << ii << std::endl;
//	hpart[0] = particle(2,2,2);
//	std::cout << hpart[0].x() << std::endl;

	for (int jj = 0; jj < _part.size()/3; jj++)
	{
		if (_part[3*jj] > cube[0] && _part[3*jj] < cube[1] && _part[3*jj+1] > cube[2] && _part[3*jj+1] < cube[3] && _part[3*jj+2] > cube[4] && _part[3*jj+2] < cube[5] )
		{
//			px = partlist[l][3*jj]-_cm[k][3*i+0];
			px = _part[3*jj]-cm[0];
			py = _part[3*jj+1]-cm[1];
			pz = _part[3*jj+2]-cm[2];
//										std::cout << std::scientific << px << "\t" << py << "\t" << pz << std::endl;
			hpart.push_back(particle(px,py,pz));
//			hpart[ii] = particle(px,py,pz);
			ii++;
		}
	}
}


int music::select_part(std::vector<particle> &hpart, int &ii, const float cm[3], const float radius)
{
	float px,py,pz;
//	std::cout << "PIPPO " << std::endl;
	for (int jj = 0; jj < _part.size()/3; jj++)
	{
		px = _part[3*jj] - cm[0];
		py = _part[3*jj+1] - cm[1];
		pz = _part[3*jj+2] - cm[2];
		if (px>2900) px = px -3000;
		if (py>2900) py = py -3000;
		if (pz>2900) pz = pz -3000;
		if (px<-2900) px = px +3000;
		if (py<-2900) py = py +3000;
		if (pz<-2900) pz = pz +3000;
		if (px < radius && px > -radius && py < radius && py > -radius && pz < radius && pz > -radius )
		{
			hpart.push_back(particle(px,py,pz));
//			hpart[ii] = particle(px,py,pz);
			ii++;
		}
	}
}


halo music::get_halo(const int id, const float cm[3], const float radius)
{
	
//	bool selected[nfiles];
//	for (int l = 0; l < nfiles; l++) selected[l] = 0;

	float cube[6];//hx0,hx1,hy0,hy1,hz0,hz1;
	cube[0] = cm[0]-radius;
	cube[1] = cm[0]+radius;
	cube[2] = cm[1]-radius;
	cube[3] = cm[1]+radius;
	cube[4] = cm[2]-radius;
	cube[5] = cm[2]+radius;
	//	int i0,i1,j0,j1,k0,k1;
	//	i0 = floor( cube[0] / 3000. * HashTabSize );
	//	i1 = floor( cube[1] / 3000. * HashTabSize );
	//	j0 = floor( cube[2] / 3000. * HashTabSize );
	//	j1 = floor( cube[3] / 3000. * HashTabSize );
	//	k0 = floor( cube[4] / 3000. * HashTabSize );
	//	k1 = floor( cube[5] / 3000. * HashTabSize );

	//	if (load_files(selected,i0,i1+1,j0,j1+1,k0,k1+1));// return 1;

	std::vector<particle> hpart;
//	hpart.resize(0);
	int ii = 0;

	//					int l = k;
//		hpart.reserve(hpart.size()+(int)(_nparticles[l]/10));
//	if (cube[0] > 0 && cube[1] < 3000 && cube[2] > 0 && cube[3] < 3000 && cube[4] > 0  && cube[5] < 3000) 
//	{
	select_part(hpart, ii, cm, cube);

		hpart.resize(ii);



	halo thalo(id,ii,radius,cm[0],cm[1],cm[2],hpart);

    
	return thalo; 
}









//void music::load_file(int k)
//{

////	int k = 0;
////	for (k = 0; id >= _files[k+1]; k++) {}
//	//{
//	//	std::cout << "Loaded file: " << _loadedfiles << "\t" << _files[k] << "\t" << _files[k+1] << "\t" << k << std::endl;
//	//}
//	

//	//	read particles file
//	std::ostringstream s2;
//	s2 << _dir << "Snapshot/snapshot/snap_music." << k;
////	s2 << _dir << "snap_music." << k;
//	std::ifstream fp(s2.str().c_str(),std::ios::binary|std::ios::in);
//	if (fp.is_open())
//	{
//		std::cout << "Opened file:\t" << s2.str() << std::endl;

//		//	reading header
//		int nread;
//		int listsize = _partlist.size();
//		_partlist.resize(listsize+1);
//		if ( k == 0)
//		{
//			fp.read((char*)&nread,4);

//			_partlist[listsize] = new float[3*nread];
//			fp.read((char*)_partlist[listsize],12*nread);
//			fp.close();
//		} else {
//			nread = _loadedparticles[0];
//			_partlist.resize(listsize+1);
//			_partlist[listsize] = _partlist[0];
//		}
//		
//		_loadedfiles.resize(listsize+1);
//		_loadedfiles[listsize] = k;

//		_loadedparticles.resize(listsize+1);
//		_loadedparticles[listsize] = nread;

//		_readedfiles.resize(listsize+1);
//		_readedfiles[listsize] = false;
//		std::cout << "Particles readed! " << nread << std::endl;


//	} else {
//		std::cout << "Could not open file " << s2.str() << "\tLoaded files " << _partlist.size() << std::endl;

//		int listsize = _partlist.size();
//		_partlist.resize(listsize+1);
//		_partlist[listsize] = 0;
//		_loadedfiles.resize(listsize+1);
//		_loadedfiles[listsize] = k;
//		_loadedparticles.resize(listsize+1);
//		_loadedparticles[listsize] = 0;
//		_readedfiles.resize(listsize+1);
//		_readedfiles[listsize] = false;
//	}

//}


//halo music::get_halo(int id,const float cm[3], const float radius)
//{
////	if (_loadedfiles >= 0)
////	{
////		if (id < _files[_loadedfiles] || id >= _files[_loadedfiles+1] )
////		{
////			std::cout << "Not in the right file..." << std::endl;
////			load_file(id);
////		}
////	}
////	else
////	{
////		std::cout << "No loaded file..." << std::endl;
////		load_file(id);
////	}
//	
//	std::cout << "Halo " << id << " Radius: " << radius << std::endl;	
//		
////	int ic,jc,kc,r0;
////	ic = ( cm[0] / 3000 * HashTabSize );
////	jc = ( cm[1] / 3000 * HashTabSize );
////	kc = ( cm[2] / 3000 * HashTabSize );
////	r0 = 3000 / HashTabSize;




//	std::vector<particle> hpart;
//	hpart.resize(10000000);
//	int ii = 0;

//	float px,py,pz,pr,x0,x1,y0,y1,z0,z1;
//	for (int i = 0; i<_readedfiles.size(); i++) _readedfiles[i] = false;
//	x0 = cm[0]-radius;
//	x1 = cm[0]+radius;
//	y0 = cm[1]-radius;
//	y1 = cm[1]+radius;
//	z0 = cm[2]-radius;
//	z1 = cm[2]+radius;

//	int i0,i1,j0,j1,k0,k1,r0;
//	i0 = ( x0 / 3000 * HashTabSize );
//	i1 = ( x1 / 3000 * HashTabSize );
//	j0 = ( y0 / 3000 * HashTabSize );
//	j1 = ( y1 / 3000 * HashTabSize );
//	k0 = ( z0 / 3000 * HashTabSize );
//	k1 = ( z1 / 3000 * HashTabSize );

////	std::cout << i0%HashTabSize << "\t" << j0 << "\t" << k0 << std::endl;
////	std::cout << x0/3000*HashTabSize << "\t" << j1 << "\t" << k1 << std::endl << std::endl;
////	std::cout << ic << "\t" << jc << "\t" << kc << std::endl;

//	for (int i=i0; i<=i1; i++)
//	{
//		for (int j=j0; j<=j1; j++)
//		{
//			for (int k=k0; k<=k1; k++)
//			{
//				int h = _Cell[( (i%HashTabSize) * HashTabSize + (j%HashTabSize) ) * HashTabSize + k%HashTabSize].Start;
//				for (h; h != -1; h = _List[h].next)
//				{
//					bool loaded = false;
//					int l;
//					for (l = 0; l < _loadedfiles.size() && !loaded; l++)
//					{
//						if (_loadedfiles[l] == _List[h].FileNr) 
//						{
//							loaded = true;
//							l--;
//						}
//					}
//					
//					if (!loaded) load_file(_List[h].FileNr);
//					
//					if (!_readedfiles[l])
//					{
////						std::cout << "File " << _List[h].FileNr <<" at index " << l << std::endl;
//						_readedfiles[l] = true;
//					
//							//	reading particle positions
////						int ii = 0;
////						if (cm[0]+radius < 3000 && cm[0]-radius > 0 && cm[1]+radius < 3000 && cm[1]-radius > 0 && cm[2]+radius < 3000 && cm[2]-radius > 0 ) 
////						{
////							for (int i = 0; i < _loadedparticles[l]; i++)
////							{
////								px = _partlist[l][3*i]-cm[0];
////								py = _partlist[l][3*i+1]-cm[1];
////								pz = _partlist[l][3*i+2]-cm[2];
////								pr = px*px + py*py + pz*pz;
////								if (pr < radius*radius) ii++;
////							}
////						} else {
////							for (int i = 0; i < _loadedparticles[l]; i++)
////							{
////								px = _partlist[l][3*i]-cm[0];
////								py = _partlist[l][3*i+1]-cm[1];
////								pz = _partlist[l][3*i+2]-cm[2];
////								if (px>2900) px = px -3000;
////								if (py>2900) py = py -3000;
////								if (pz>2900) pz = pz -3000;
////								if (px<-2900) px = px +3000;
////								if (py<-2900) py = py +3000;
////								if (pz<-2900) pz = pz +3000;
////								pr = px*px + py*py + pz*pz;
////								if (pr < radius*radius) ii++;
////							}
////						}

//						//std::vector<particle> hpart(_mass[id]);
////						int hpartsize = hpart.size();
////						hpart.resize(hpartsize+ii);
////						ii = hpartsize;
//	
//						
//						if (cm[0]+radius < 3000 && cm[0]-radius > 0 && cm[1]+radius < 3000 && cm[1]-radius > 0 && cm[2]+radius < 3000 && cm[2]-radius > 0 ) 
//						{
//							std::cout << "Loop " << _loadedparticles[l] << " times" << std::endl;	
//							for (int jj = 0; jj < _loadedparticles[l]; jj++)
//							{
//								if (_partlist[l][3*jj] > x0 && _partlist[l][3*jj] < x1 &&_partlist[l][3*jj+1] > y0 &&_partlist[l][3*jj+1] < y1 &&_partlist[l][3*jj+2] > z0 && _partlist[l][3*jj+2] < z1 )
//								{
//									px = _partlist[l][3*jj]-cm[0];
//									py = _partlist[l][3*jj+1]-cm[1];
//									pz = _partlist[l][3*jj+2]-cm[2];
//									hpart[ii] = particle(px,py,pz);
//									ii++;
//								}
//							}
//						} else {
//							for (int jj = 0; jj < _loadedparticles[l]; jj++)
//							{
//								px = _partlist[l][3*jj]-cm[0];
//								py = _partlist[l][3*jj+1]-cm[1];
//								pz = _partlist[l][3*jj+2]-cm[2];
//								if (px>2900) px = px -3000;
//								if (py>2900) py = py -3000;
//								if (pz>2900) pz = pz -3000;
//								if (px<-2900) px = px +3000;
//								if (py<-2900) py = py +3000;
//								if (pz<-2900) pz = pz +3000;
//								if (px < radius && px > -radius && py < radius && py > -radius && pz < radius && pz > -radius )
//								{
//									hpart[ii] = particle(px,py,pz);
//									ii++;
//								}
//							}
//						}
//						
//						
//					}
//					

//				}
//			}
//		}
//	}
//	
//	
////	for (int k=0; k<_loadedfiles.size(); k++)
////	{
////	}

////	for (int k=1; k<_loadedfiles.size(); k++)
////	{
////		std::cout << "Unloading file " << _loadedfiles[k] << std::endl;
////		delete _partlist[k];
////	}
////	_partlist.resize(1);
////	_loadedfiles.resize(1);

//	hpart.resize(ii);

//	halo halo1(id,ii,cm[0],cm[1],cm[2],hpart);
//	
//	return halo1;
//}

//halo music::get_halo(int id)
//{
//		float cm[3];
//		cm[0]=_cm[3*id];
//		cm[1]=_cm[3*id+1];
//		cm[2]=_cm[3*id+2];
//		
////		cm[0] = 3000 /HashTabSize /2;
////		cm[1] = 3000 /HashTabSize /2;
////		cm[2] = 3000 /HashTabSize -0.1;

//		
//////		z = 0.
//////		Hubble = 100.
//////		Omega0 = 0.25
//////		G = 43.0071
////		rho_back = 
//////		float DeltaTopHat = (18 * !PI * !PI + 82 * (Omega0-1) - 39 * x * x)/omega0;
//////		rvir = ( _mass[id] / DeltaTopHat / rho_back   )^(1./3)
////		std::cout << (_mass[id] /200 / ( 0.25 * 100000.) * 2 * 43.0071 )**3 << std::endl;
////		
////		rho_back = 3 * Omega0 * 10000 / (8 * np.pi * G)
////		r200 = cbrt(2*43.0071*_mass[id] / (200*0.25*10000) )

//		return get_halo(id,cm,1.2*cbrt(2*43.0071*_mass[id] / (200*0.25*10000) ));
//}
