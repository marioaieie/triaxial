#ifndef HALO_HH
#define HALO_HH

#include "particle.hh"
#include <vector>
#include <iostream>

class halo {
	public:
//		Constructors and distructors
		halo(int, int, float, float, float, float, std::vector<particle>);
		halo(int, int, float, float, float, float, float[3], float[3][3], std::vector<particle>);
		halo();

//		Interesting...
		void mtensor();
		int reshape(float delta_step);						// For shape
		int reshape(float, float[3]) const;		// For centres finder
		void resize();
		int denprofile(float, float, int, float*, float*);
		float rcri() {}
		void compute_mtensor();
	//	const particle* list_add() { return &_list[1];}

//		Boring stuff...
		int id() const {return _id;}
		int mass() const {return _mass;}
		float radius() const {return _radius;}
		int m200() const {return _m200;}
		int mcri() const {return _mcri;}
		void eigenvalues(float[3]) const;
		void eigenvectors(float[3][3]) const;
		float xpart(int i) const { return _list[i].x(); }
		float ypart(int i) const { return _list[i].y(); }
		float zpart(int i) const { return _list[i].z(); }
		float rpart(int i) const { return _list[i].r(); }
		float npart() const { return _list.size(); }
		float x() const {return _x;}
		float y() const {return _y;}
		float z() const {return _z;}
		float dum();
//		void cm(int,float[3]) const;
		void write(std::ostream&);
		friend std::ostream& operator<<(std::ostream& os, const halo& self);

	private:
		int _id, _m200, _mcri;
		float _mxx, _myy, _mzz, _mxy, _mxz, _myz, _x, _y, _z, _radius;
		float _axis[3];
		float _evec[3][3];
		float _delta;
		double _mass;
		
		std::vector<particle> _list;
};
#endif
