#ifndef PARTICLE_HH
#define PARTICLE_HH
#include <iostream>
#include <cmath>

class particle {
	public:
		particle() : _x(0), _y(0), _z(0), _r(0), _mass(0) { }
		particle(float x, float y, float z, float mass) : _x(x), _y(y), _z(z), _mass(mass) {_r=x*x + y*y + z*z;}
		particle(float x, float y, float z, float r, float mass) : _x(x), _y(y), _z(z), _r(r), _mass(mass) { }
		~particle() {}
		void coord() const {std::cout << _x <<"\t" << _y <<"\t" << _z << std::endl;}
		float x() const {return _x;}
		float y() const {return _y;}
		float z() const {return _z;}
		float r() const {return _r;}
		float mass() const {return _mass;}
	    bool operator<(particle const &other) const { return _r < other._r;}
	private:
	float _x, _y, _z, _r, _mass;
};
#endif
