#include <iostream>
#include <fstream>
#include <iomanip> // setprecision()
#include <limits>
#include <cmath>
#include <sstream>
#include <vector>
#include <omp.h>
#include <cstdlib> // random
#include "particle.hh"
#include "halo.hh"
#include "cosmo.hh"
#include "/home/Software/Unsio-1.0.1/inc/uns/uns.h"


template<typename T>
std::ostream& binary_write(std::ostream& stream, const T& value){
    return stream.write(reinterpret_cast<const char*>(&value), sizeof(T));
}
void writeonfile(std::ofstream& fileout,halo& thalo)
{				
	float tcm[3] = {thalo.x(),thalo.y(),thalo.z()};
	float eigval[3],eigvec[3][3];
	thalo.eigenvectors(eigvec);
	thalo.eigenvalues(eigval);
	binary_write(fileout,thalo.id());
	binary_write(fileout,thalo.mass());
	binary_write(fileout,thalo.radius());
	binary_write(fileout,tcm);
	binary_write(fileout,eigval);
	binary_write(fileout,eigvec);
//	binary_write(fileout,1);
}

struct struct_cell
{
   int Start;
   int Last;
} *Cell;

struct struct_list
{
  int FileNr;
  int next;
} *List;

const std::string _dir = "/data/mbonamigo/Mxxl/";
//std::string _dir = "/home/data/Mxxl/";
const int nfiles = 3072;
const int snap = 41;
bool _inside[nfiles];
int box;

// Hash stuff
int HashTabSize;
struct_cell *_Cell;
struct_list *_List;

// Header stuff
int _nhaloes[nfiles];
int *_len[nfiles];
int *_off[nfiles];
float *_cm[nfiles];
float *_m200[nfiles];
float *_mcri[nfiles];

// Particles stuff
int _nparticles[nfiles];
float *_partlist[nfiles];
//bool _partlist[nfiles];

// Shape stuff
//const int nlines2 = 68708475;  // 63
const int nlines2 = 67378786;  // 41
float _mass[nlines2];
float _cm2[nlines2][3];
int _id2[nlines2];
const int nlines = 781059;
int _id[nlines];
int _m200n[nlines];
float _rvir[nlines];
float _axis[nlines][3];
float _evec[nlines][3][3];

// Conc stuff
int nbin = 32;

void load_hash()
{

	std::ostringstream s;
	s << _dir << "hashtable_" << std::setw(3) << std::setfill('0') << snap << ".dat";
	std::ifstream fhash(s.str().c_str(),std::ios::binary|std::ios::in);
	std::cout <<s.str()<< std::endl;

	fhash.read((char*)&HashTabSize,sizeof(int));
	std::cout << "HashTabSize:" << "\t" << HashTabSize << std::endl;

	_Cell = new struct_cell[HashTabSize * HashTabSize * HashTabSize];
	fhash.read((char*)_Cell,HashTabSize * HashTabSize * HashTabSize*sizeof(struct_cell));
//	std::cout << "Cell[0].Start:" << "\t" << _Cell[0].Start << std::endl;

	int numlist;
	fhash.read((char*)&numlist,sizeof(int));
//	std::cout << "numlist:" << "\t" << numlist << std::endl;

	_List = new struct_list[numlist];
	fhash.read( (char*)_List, numlist*sizeof(struct_list));

	fhash.close();
}


int load_head(int k)
{
	std::ostringstream s;
	s << _dir << "Fof/halo." << k;
	std::ifstream fh(s.str().c_str(),std::ios::binary|std::ios::in);
//		c++ 11
//		ifstream f1("halo"+to_string(j),,ios::binary|ios::in);
//	std::cout << "Opened file "<< s.str();
	if (fh.is_open())
	{
//		std::cout << "Opened file:\t" << s.str() << std::endl;


		int nhalo;
		fh.read((char*)&nhalo,4);

//		int halooff[nhalo];
		float *halocm = new float[3*nhalo];
		_len[k] = new int[nhalo];
		_off[k] = new int[nhalo];
		_cm[k] = new float[3*nhalo];
		_m200[k] = new float[nhalo];
		_mcri[k] = new float[nhalo];

		fh.read((char*)_len[k],4*nhalo);
		fh.read((char*)_off[k],4*nhalo);
		fh.read((char*)halocm,12*nhalo);
		fh.read((char*)_cm[k],12*nhalo);
		fh.read((char*)_m200[k],4*nhalo);
		fh.read((char*)_mcri[k],4*nhalo);

		_nhaloes[k] = nhalo;
		
		fh.close();
	
	} else {
		std::cout << "Could not open file " << s.str() << std::endl;
		return 1;
	}

	return 0;

}

int load_catalouge()
{
	std::ostringstream s;
	s << _dir << "Fof/vhalos_" << std::setw(3) << std::setfill('0') << snap << "_1e12.dat";
	std::ifstream fc(s.str().c_str(),std::ios::binary|std::ios::in);

	if (fc.is_open())
	{
		
		int nhalo;
		fc.read((char*)&nhalo,4);
		std::cout << "nhalo:" << "\t" << nhalo << std::endl;

		float vmax;
		for (int i = 0; i < nhalo; i++)
		{
			fc.read((char*)_cm2[i],12);
			fc.read((char*)&_mass[i],4);
			fc.read((char*)&vmax,4);
		}
		fc.close();
	
	} else {
		std::cout << "Could not open file " << s.str() << std::endl;
		return 1;
	}

	std::cout << "Catalouge read correctly." << std::endl;

	return 0;
	
}

int load_snap(int k)
{

	//	read particles file
	std::ostringstream s2;
	s2 << _dir << "Snapshot_" << std::setw(3) << std::setfill('0') << snap << "/snap_mxxl." << k;
	std::ifstream fp(s2.str().c_str(),std::ios::binary|std::ios::in);
	if (fp.is_open())
	{
//		std::cout << "Opened file:\t" << s2.str() << std::endl;

		//	reading header
		int nread;
		fp.read((char*)&nread,4);
		_nparticles[k] = nread;

		_partlist[k] = new float[3*nread];
		fp.read((char*)_partlist[k],12*nread);
		fp.close();

//		std::cout << "Particles readed! " << nread << std::endl;

	} else {
		std::cout << "Could not open file " << s2.str() << std::endl;
		return 1;
	}

	return 0;
}


int load_files(bool files[nfiles], int x0, int x1, int y0, int y1,int z0, int z1)
{
//			std::cout << "WTF?? " << x0 << "\t" << x1 << "\t" << y0 << "\t" << y1 << "\t" << z0 << "\t" << z1 << std::endl;


	for (int ix = x0; ix < x1; ix++)
	{
		for (int iy = y0; iy < y1; iy++)
		{
			for (int iz = z0; iz < z1; iz++)
			{
//			std::cout << (ix+256)%HashTabSize << "\t" << (iy+256)%HashTabSize << "\t" << (iz+256)%HashTabSize << std::endl;
			int h = _Cell[( ((ix+256)%HashTabSize) * HashTabSize + ((iy+256)%HashTabSize) ) * HashTabSize + ((iz+256)%HashTabSize)].Start;
				for (h; h != -1; h = _List[h].next)
				{
				// File to load	
//					if (!files[_List[h].FileNr])
//					{

					files[_List[h].FileNr] = 1;

//					} else {
//						if (files[_List[h].FileNr] == -1) files[_List[h].FileNr] = 1;
//					}
				}
			}	
		}
	}
	
	return 0;
}

int load_shapes()
{
	std::ostringstream ss,se;
//	ss << _dir << "halo_SO_snap_real.dat";
	se << _dir << "halo_EO_snap_real.dat";
//	std::ifstream fs(ss.str().c_str(),std::ios::in);
	std::ifstream fe(se.str().c_str(),std::ios::in);
	int id;
	int idum;
	float rdum;//, c, b, a, c0, c1, c2, b0, b1, b2, a0, a1, a2;

	for (int i = 0; i < nlines; i++)
	{
		fe >> id >> idum >> _m200n[i] >> _rvir[i] >> rdum >> rdum >> rdum
//		>> c >> b >> a >> c0 >> c1 >> c2 >> b0 >> b1 >> b2 >> a0 >> a1 >> a2 >> idum;
		>> _axis[i][0] >> _axis[i][1] >> _axis[i][2]
		>> _evec[i][0][0] >> _evec[i][1][0] >> _evec[i][2][0]
		>> _evec[i][0][1] >> _evec[i][1][1] >> _evec[i][2][1]
		>> _evec[i][0][2] >> _evec[i][1][2] >> _evec[i][2][2] >> idum;
		
		_id[id] = i;

	}
	return 0;	
}

int read_part(std::vector<particle> &hpart, int &ii, float *partlist, int jjstart, int jjend, float cm[3], float cube[6])
{
	float px,py,pz;
//	std::cout << hpart[0].x() << std::endl;
//	std::cout << ii << std::endl;
//	hpart[0] = particle(2,2,2);
//	std::cout << hpart[0].x() << std::endl;
	for (int jj = jjstart; jj < jjend; jj++)
	{
		if (partlist[3*jj] > cube[0] && partlist[3*jj] < cube[1] && partlist[3*jj+1] > cube[2] && partlist[3*jj+1] < cube[3] && partlist[3*jj+2] > cube[4] && partlist[3*jj+2] < cube[5] )
		{
//			px = partlist[l][3*jj]-_cm[k][3*i+0];
			px = partlist[3*jj]-cm[0];
			py = partlist[3*jj+1]-cm[1];
			pz = partlist[3*jj+2]-cm[2];
//										std::cout << std::scientific << px << "\t" << py << "\t" << pz << std::endl;
			hpart.push_back(particle(px,py,pz));
//			hpart[ii] = particle(px,py,pz);
			ii++;
		}
	}
}


int read_part(std::vector<particle> &hpart, int &ii, float *partlist, int jjstart, int jjend, float cm[3], float radius)
{
	float px,py,pz;
//	std::cout << "PIPPO " << std::endl;
	for (int jj = jjstart; jj < jjend; jj++)
	{
		px = partlist[3*jj] - cm[0];
		py = partlist[3*jj+1] - cm[1];
		pz = partlist[3*jj+2] - cm[2];
		if (px>2900) px = px -3000;
		if (py>2900) py = py -3000;
		if (pz>2900) pz = pz -3000;
		if (px<-2900) px = px +3000;
		if (py<-2900) py = py +3000;
		if (pz<-2900) pz = pz +3000;
		if (px < radius && px > -radius && py < radius && py > -radius && pz < radius && pz > -radius )
		{
			hpart.push_back(particle(px,py,pz));
//			hpart[ii] = particle(px,py,pz);
			ii++;
		}
	}

}


int get_shape(halo &thalo, float ndelta)
{

	float eig_halo[3];
	float s,q,s1,q1;
	thalo.eigenvalues(eig_halo);
	q1 = sqrt(eig_halo[1]/eig_halo[0]);
	s1 = sqrt(eig_halo[2]/eig_halo[0]);

	int it=0;
	do 
	{
		s = s1;
		q = q1;
//		f11 << it << '\t' << s << '\t' << q << std::endl;

		thalo.reshape(ndelta);

		thalo.eigenvalues(eig_halo);
		q1 = sqrt(eig_halo[1]/eig_halo[0]);
		s1 = sqrt(eig_halo[2]/eig_halo[0]);

		it++;
		if (thalo.mass() < 1000) return -it;

//		} while ( ( (s1-s)*(s1-s)/(s*s) > 0.005 || (q1-q)*(q1-q)/(q*q) > 0.005 ) && it<100);
	} while ( ( std::abs(s1-s)/s > 0.005 || std::abs(q1-q)/q > 0.005 ) && it<100);
//		} while ((std::abs( thalo.rpart(thalo.mass())-rell) /rell) > 0.0001 && it<100);

		return it;
}


//int get_conc(halo thalo)
//{

//	float rcri = thalo.rcri();
////	std::cout << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << "\t" << cbrt( thalo.mcri()*0.61735814809799194 / cosmo::delta_c/ cosmo::rho_back / (4.*M_PI/3.)  ) << std::endl;

//	float density[nbin];
//	float rmean[nbin];
//	for (int jj = 0; jj < nbin; jj++) 
//	{
//		density[jj] = 0;
//		rmean[jj] = 0;
//	}
//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
//	if ( pippo ) 
//	{
//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
//		return 1;
//	}


////	std::cout << "rmean = [";
////	for (int jj = 0; jj < nbin-1; jj++)
////	{
////		std::cout << rmean[jj] << ", ";
////	}
////	std::cout << rmean[nbin-1] << "]" << std::endl;

////	std::cout << "dens = [";
////		for (int jj = 0; jj < nbin-1; jj++)
////	  {
////		std::cout << density[jj] << ", ";
////	  }
////	std::cout << density[nbin-1] << "]" << std::endl;

////	fprof.write((char*)rmean,4*nbin);
////	fprof.write((char*)density,4*nbin);
//	return 0;
//}



int main(int argc, char* argv[])
{

	load_hash();
	int num = 0;
	box = 17;
	int count = 0;
	int maxram = 0;
	int ram;
	int numhalo = 0;
	const float rscale = 3. * 1.8*cbrt(cosmo::G*cosmo::omegaz / cosmo::Omega0) /100.;

	for (int k = 0; k < nlines; k++)
	{
		_rvir[k] = 0;
		_axis[k][0] = 0;
		_axis[k][1] = 0;
		_axis[k][2] = 0;
		_evec[k][0][0] = 0;
		_evec[k][0][1] = 0;
		_evec[k][0][2] = 0;
		_evec[k][1][0] = 0;
		_evec[k][1][1] = 0;
		_evec[k][1][2] = 0;
		_evec[k][2][0] = 0;
		_evec[k][2][1] = 0;
		_evec[k][2][2] = 0;
//		_todo[k] = 0;
		_id[k] = -1;
	}

	int haloindex[nfiles+1];
	haloindex[0] = 0;
	
	for (int k = 0; k < nfiles; k++)
	{
		_inside[k] = 0;
		_nhaloes[k] = 0;
		_cm[k] = 0;
		_m200[k] = 0;
		_mcri[k] = 0;
		if (load_head(k)) return 1;
		haloindex[k+1] = haloindex[k] + _nhaloes[k];
	}


//	std::ofstream fprof("density_profiles_EO",std::ios::binary|std::ios::out);
//	std::ofstream fprofs("density_profiles_SO",std::ios::binary|std::ios::out);

	//#pragma omp master
	int nthread = omp_get_max_threads();
	std::cout << "Number of threads:\t" << nthread << std::endl;


	load_catalouge();

//	std::cout << _cm2[0][0] << std::endl;

	std::ostringstream ssel;
	ssel << "random_haloes_" << std::setw(3) << std::setfill('0') << snap << ".txt";
	std::ifstream randomh(ssel.str().c_str(),std::ios::in);
	int tind,ntot;
	char dum[256];
	ntot = 0;
	while (randomh.getline(dum,256)) ntot++;
	std::cout << "Found "<< ntot << " haloes" << std::endl;
	randomh.clear();
	randomh.seekg (0, randomh.beg);
//	ntot = 1090196;  // 41
//	ntot = 1000;
//	ntot = nlines2;
	int _todo[ntot];
	for (int i = 0; i < ntot; i++)
	{
		randomh >> _todo[i];
//		_todo[i] = i;
	}
										//	load_shapes();


	//	int ipippo = 9;
	//	int kpippo = 10;
	//	std::cout << haloindex[kpippo]+ipippo << "\t" << _axis[_id[haloindex[kpippo]+ipippo]][0] << std::endl;
	//	return 0;

	int xlim,ylim,zlim;
	xlim = 256;
	ylim = 256;
	zlim = 256;

//	if (argc-1 == 0)
//	{
//		xlim,ylim,zlim = 0;
//	}
//	else
//		if (argc-1 == 3)
//		{	
//			xlim = std::atoi(argv[1]);
//			ylim = std::atoi(argv[2]);
//			zlim = std::atoi(argv[3]);
//		}
//		else
//		{
//			std::cout << "Incorrect number of arguments (" << argc << "). Needed 0 or 3" << std::endl;
//			return 0;
//		}

	
	int outside = 0;
	for (int xc = 0; xc < xlim; xc+=box)
	{
		std::ofstream fso[nthread],feo[nthread];
		for (int i = 0; i < nthread; i++)
		{
			std::ostringstream sso,seo;
			sso << "halo_SO_prof_" << std::setw(3) << std::setfill('0') << snap << "_" << nthread*int(xc/box)+i << ".dat";
			seo << "halo_EO_prof_" << std::setw(3) << std::setfill('0') << snap << "_" << nthread*int(xc/box)+i << ".dat";
			fso[i].open(sso.str().c_str(),std::ios::binary|std::ios::out);
			feo[i].open(seo.str().c_str(),std::ios::binary|std::ios::out);
		}
		for (int yc = 0; yc < ylim; yc+=box)
		{
			for (int zc = 0; zc < zlim; zc+=box)
			{
//	for (int xc = 170; xc < 180; xc+=box)
//	{
//		for (int yc = 45; yc < 55; yc+=box)
//		{
//			for (int zc = 195; zc < 205; zc+=box)
//			{

	//		INSIDE BIG CUBE
	int xm = xc + box;
	if (xm>256) xm = 256;
	int ym = yc + box;
	if (ym>256) ym = 256;
	int zm = zc + box;
	if (zm>256) zm = 256;
	
	float x0,x1,y0,y1,z0,z1;
	x0 = xc *3000. / HashTabSize;
	x1 = xm *3000. / HashTabSize;
	y0 = yc *3000. / HashTabSize;
	y1 = ym *3000. / HashTabSize;
	z0 = zc *3000. / HashTabSize;
	z1 = zm *3000. / HashTabSize;
				std::cout << "Doing:\t"<< x0 << "\t" << x1 << "\t" << y0 << "\t" << y1 << "\t" << z0 << "\t" << z1 << "\t";


	for (int k = 0; k < nfiles; k++) _inside[k] = 0;
	if (load_files(_inside,xc,xm,yc,ym,zc,zm)) return 1;


	bool needed[nfiles];
	for (int k = 0; k < nfiles; k++) needed[k] = 0;
	
	for (int i = 0; i < ntot; i++)
	{

		float cm[3];
		cm[0] = _cm2[_todo[i]][0];
		cm[1] = _cm2[_todo[i]][1];
		cm[2] = _cm2[_todo[i]][2];
//		std::cout << i << "\t" << "cm: " << cm[0] << "\t" << cm[1] << "\t" << cm[2] << std::endl;
	
		if (cm[0] <= x1 && cm[0] > x0 && cm[1] <= y1 && cm[1] > y0 && cm[2] <= z1 && cm[2] > z0 )
		{
//		std::cout << i << "\t" << "cm: " << cm[0] << "\t" << cm[1] << "\t" << cm[2] ;//<< std::endl;

			// 1.83 @ z=0
			float radius = rscale*cbrt(_mass[_todo[i]]);
//						if (_id[haloindex[k]+i] == -1)
//						{
//							std::cout << "mmm... sth smells here... " << haloindex[k]+i << std::endl;
//							return 1;
//						}
//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
		
			int i0,i1,j0,j1,k0,k1;
			i0 = floor( (cm[0]-radius) / 3000. * HashTabSize );
			i1 = floor( (cm[0]+radius) / 3000. * HashTabSize );
			j0 = floor( (cm[1]-radius) / 3000. * HashTabSize );
			j1 = floor( (cm[1]+radius) / 3000. * HashTabSize );
			k0 = floor( (cm[2]-radius) / 3000. * HashTabSize );
			k1 = floor( (cm[2]+radius) / 3000. * HashTabSize );

//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
			if (load_files(needed,i0,i1+1,j0,j1+1,k0,k1+1)) return 1;
		}
		
	}

//	std::cout << std::endl;
	
//	std::cout << "Inside: \t";
//	for (int k = 0; k < nfiles; k++) if (_inside[k]) std::cout << k << "\t";
//	std::cout << std::endl;

//	std::cout << "Needed: \t";
	ram = 0;
	for (int k = 0; k < nfiles; k++) 
	{
		if (needed[k])
		{
//			std::cout << k << "\t";
			ram++;
		}
	}
//	std::cout << std::endl;
	if (1.2*ram>30)
	{
//		std::cout << "Too much RAM!!!";
		return 1;
	}

//	std::cout << "Partlist: \t";
//	for (int k = 0; k < nfiles; k++) if (_partlist[k]) std::cout << k << "\t";
//	std::cout << std::endl;

	for (int k = 0; k < nfiles; k++) 
	{
		if (_partlist[k] && !needed[k] && !_inside[k])
		{
//			std::cout << "Unloading file " << k << std::endl;
			delete _partlist[k];
			_partlist[k] = 0;
		}
	}
	for (int k = 0; k < nfiles; k++) 
	{
		if (!_partlist[k] && needed[k])			
		{
			// Load files
//			std::cout << "Loading file " << k << std::endl;
			count++;
			if (load_snap(k)) return 1;	// _partlist[k] = 1;
		}
	}

	std::cout << "Loading, DONE!" << std::endl;
	
	
//			// Do your stuff...
	#pragma omp parallel for default(shared)
//	#pragma omp parallel for default(shared) reduction(+:numhalo)
	for (int i = 0; i < ntot; i++)
	{
		float cm[3];
		cm[0] = _cm2[_todo[i]][0];
		cm[1] = _cm2[_todo[i]][1];
		cm[2] = _cm2[_todo[i]][2];
		
	
		if (cm[0] <= x1 && cm[0] > x0 && cm[1] <= y1 && cm[1] > y0 && cm[2] <= z1 && cm[2] > z0 )// if (_mass[_todo[i]]>500)
		{
	
//				numhalo++;
			//	fso[omp_get_thread_num()] << _todo[i] << "\tThread: " <<  omp_get_thread_num()<< std::endl;
			
			
//		\/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/


			bool selected[nfiles];
			for (int l = 0; l < nfiles; l++) selected[l] = 0;

			float radius = rscale*cbrt(_mass[_todo[i]]);
//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
			float cube[6];//hx0,hx1,hy0,hy1,hz0,hz1;
			cube[0] = cm[0]-radius;
			cube[1] = cm[0]+radius;
			cube[2] = cm[1]-radius;
			cube[3] = cm[1]+radius;
			cube[4] = cm[2]-radius;
			cube[5] = cm[2]+radius;
			int i0,i1,j0,j1,k0,k1;
			i0 = floor( cube[0] / 3000. * HashTabSize );
			i1 = floor( cube[1] / 3000. * HashTabSize );
			j0 = floor( cube[2] / 3000. * HashTabSize );
			j1 = floor( cube[3] / 3000. * HashTabSize );
			k0 = floor( cube[4] / 3000. * HashTabSize );
			k1 = floor( cube[5] / 3000. * HashTabSize );
//					std::cout << "\tcm: " << k << "\t" << i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << "\t" << radius << std::endl;
//					std::cout << "\tcube: " << cube[0] << "\t" << cube[1] << "\t" << cube[2] << "\t" << cube[3] << "\t" << cube[4] << "\t" << cube[5] << std::endl;
//					std::cout << "\tcube: " << cube[0]/ 3000. * HashTabSize << "\t" << cube[1]/ 3000. * HashTabSize << "\t" << cube[2]/ 3000. * HashTabSize << "\t" << cube[3]/ 3000. * HashTabSize << "\t" << cube[4] / 3000. * HashTabSize<< "\t" << cube[5] / 3000. * HashTabSize << std::endl;
//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
//					std::cout << "\thash: " << (-i0)%HashTabSize << "\t" << i0%HashTabSize << "\t" << (-j0)%HashTabSize << "\t" << j0%HashTabSize << "\t" << (-k0)%HashTabSize << "\t" << k0%HashTabSize  << std::endl;

//					std::cout << "File " << k << "\tHalo " << i << "\tRadius "<< radius << std::endl;
			if (load_files(selected,i0,i1+1,j0,j1+1,k0,k1+1));// return 1;
//	std::cout << "selected: \t";
//	for (int l = 0; l < nfiles; l++) if (selected[l]) std::cout << l << "\t";
//	std::cout << std::endl;
//						return 0;

			std::vector<particle> hpart;
			hpart.resize(0);
			int ii = 0;

//					int l = k;
			for (int l = 0; l < nfiles; l++)
			{
				if (selected[l])
				{

					float px,py,pz;
					hpart.reserve(hpart.size()+(int)(_nparticles[l]/10));
					if (cube[0] > 0 && cube[1] < 3000 && cube[2] > 0 && cube[3] < 3000 && cube[4] > 0  && cube[5] < 3000) 
					{
//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, cube);
						read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, cube);
					} else {
//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, radius);
						read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, radius);								
					}

					hpart.resize(ii);
				}

			}

//					 COMPUTE SHAPES
			float dens[10];
			for (int nn = 0; nn < 7; nn++) dens[nn] = pow(2.,nn);
			dens[7] = -200./cosmo::omegaz;
			dens[8] = -500./cosmo::omegaz;
			dens[9] = -2500./cosmo::omegaz;

			for (int nn = 0; nn < 10; nn++){
				halo thalo(_todo[i],ii,radius,cm[0],cm[1],cm[2],hpart);
				thalo.reshape(dens[nn]);
				writeonfile(fso[omp_get_thread_num()],thalo);
				binary_write(fso[omp_get_thread_num()],0);
				int it = 0;
				if (thalo.mass() >= 1000) it = get_shape(thalo,dens[nn]);
				writeonfile(feo[omp_get_thread_num()],thalo);
				binary_write(feo[omp_get_thread_num()],it);
			}






			
//			for (int nn = 0; nn < 3; nn++){
//				halo thalo(_todo[i],ii,radius,cm[0],cm[1],cm[2],hpart);
//				thalo.reshape(-spec_radii[nn]/cosmo::omegaz);
////				binary_write(fso[omp_get_thread_num()],spec_radii[nn]);
//				writeonfile(fso[omp_get_thread_num()],thalo);
//				binary_write(fso[omp_get_thread_num()],0);
////				fso[omp_get_thread_num()] << thalo << std::endl;
//				if (thalo.mass() >= 1000)
//				{
//					int it = get_shape(thalo,-spec_radii[nn]/cosmo::omegaz);
////					binary_write(feo[omp_get_thread_num()],spec_radii[nn]);
//					writeonfile(feo[omp_get_thread_num()],thalo);
//					binary_write(feo[omp_get_thread_num()],it);
////					feo[omp_get_thread_num()] << thalo << '\t' << it << std::endl;
//				} else {
//					writeonfile(feo[omp_get_thread_num()],halo());
//					binary_write(feo[omp_get_thread_num()],-1);
////					feo[omp_get_thread_num()] << halo() << '\t' << -1 << std::endl;					
//				}
//			}

	//		halo thalo(_todo[i],ii,radius,cm[0],cm[1],cm[2],hpart);
	//		thalo.reshape(1.);
	//		fso[omp_get_thread_num()] << thalo << std::endl;

	//		float eig_halo[3];
	//		float s,q,s1,q1;
	//		thalo.eigenvalues(eig_halo);
	//		q1 = sqrt(eig_halo[1]/eig_halo[0]);
	//		s1 = sqrt(eig_halo[2]/eig_halo[0]);

	//		int it=0;
	//		do 
	//		{
	//			s = s1;
	//			q = q1;

	//			thalo.reshape2();

	//			thalo.eigenvalues(eig_halo);
	//			q1 = sqrt(eig_halo[1]/eig_halo[0]);
	//			s1 = sqrt(eig_halo[2]/eig_halo[0]);

	//			it++;

	//		} while ( ( std::abs(s1-s)/s > 0.005 || std::abs(q1-q)/q > 0.005 ) && it<100);


//			int it = get_shape(thalo);
	//		feo[omp_get_thread_num()] << thalo << '\t' << it << std::endl;





//						fso << haloindex[k]+i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << std::endl;


//			std::cout << omp_get_thread_num() << std::endl;
//			fso[omp_get_thread_num()] << _todo[i] << std::endl;
//			feo[omp_get_thread_num()] << _todo[i] << std::endl;





		}
	}


	if (ram > maxram) maxram = ram;
//	std::cout << "Transfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;

			}
		}

	for (int i; i<nthread; i++)
	{
		fso[i].close();
		feo[i].close();	
	}

	}	
	std::cout << "Box : " << box << "\tTransfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;


//	fso.close();
//	feo.close();
//	fprofs.close();
//	fprof.close();

	return 0;
}	
	
	
	

//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (_inside[k])
//		{
//			// Do your stuff...
////			std::cout << "I'm doing my stuff... " << _nhaloes[k] << std::endl;
////		if (load_head(k)) return 1;

//			for (int i = 0; i < _nhaloes[k]; i++)
//			{

//				if (_todo[haloindex[k]+i])
//				{
//					if (_cm[k][3*i] <= x1 && _cm[k][3*i] > x0 && _cm[k][3*i+1] <= y1 && _cm[k][3*i+1] > y0 && _cm[k][3*i+2] <= z1 && _cm[k][3*i+2] > z0 )
//					{
//						float radius = 3 *cbrt(2*43.0071*_m200[k][i] / (200*0.25*10000));
////						if (_id[haloindex[k]+i] == -1)
////						{
////							std::cout << "mmm... sth smells here... " << haloindex[k]+i << std::endl;
////							return 1;
////						}
//	//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
//					
//						int i0,i1,j0,j1,k0,k1;
//						i0 = floor( (_cm[k][3*i]-radius) / 3000. * HashTabSize );
//						i1 = floor( (_cm[k][3*i]+radius) / 3000. * HashTabSize );
//						j0 = floor( (_cm[k][3*i+1]-radius) / 3000. * HashTabSize );
//						j1 = floor( (_cm[k][3*i+1]+radius) / 3000. * HashTabSize );
//						k0 = floor( (_cm[k][3*i+2]-radius) / 3000. * HashTabSize );
//						k1 = floor( (_cm[k][3*i+2]+radius) / 3000. * HashTabSize );
//	
//	//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
//						if (load_files(needed,i0,i1+1,j0,j1+1,k0,k1+1)) return 1;
//					}
//				}
//			}
//		}
//	}

////	std::cout << "Inside: \t";
////	for (int k = 0; k < nfiles; k++) if (_inside[k]) std::cout << k << "\t";
////	std::cout << std::endl;

////	std::cout << "Needed: \t";
//	ram = 0;
//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (needed[k])
//		{
////			std::cout << k << "\t";
//			ram++;
//		}
//	}
////	std::cout << std::endl;
//	if (ram>30)
//	{
//		std::cout << "Too much RAM!!!";
//		return 1;
//	}

////	std::cout << "Partlist: \t";
////	for (int k = 0; k < nfiles; k++) if (_partlist[k]) std::cout << k << "\t";
////	std::cout << std::endl;

//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (_partlist[k] && !needed[k] && !_inside[k])
//		{
////			std::cout << "Unloading file " << k << std::endl;
////			delete _partlist[k];
//			_partlist[k] = 0;
//		}
//	}
//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (!_partlist[k] && needed[k])			
//		{
//			// Load files
////			std::cout << "Loading file " << k << std::endl;
//			count++;
////			if (load_snap(k)) return 1;
////			_partlist[k] = 1;
//		}
//	}

////	std::cout << "Loading, DONE!" << std::endl;

//	for (int k = 0; k < nfiles; k++) 
//	{
////		if (files[l]) std::cout << l*files[l] << "\t";
//		if (_inside[k])
//		{
//			// Do your stuff...
////			#pragma omp parallel for default(shared) reduction(+:numhalo)
//			for (int i = 0; i < _nhaloes[k]; i++)
//			{
//				if (_todo[haloindex[k]+i])
//				{
//					float cm[3];
//					cm[0] = _cm[k][3*i+0];
//					cm[1] = _cm[k][3*i+1];
//					cm[2] = _cm[k][3*i+2];
//				
//					if (cm[0] <= x1 && cm[0] > x0 && cm[1] <= y1 && cm[1] > y0 && cm[2] <= z1 && cm[2] > z0 )
//					{
//				
//							numhalo++;
//							std::cout << haloindex[k]+i << std::endl;
//						
//						
//	//		\/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/

////						bool selected[nfiles];
////						for (int l = 0; l < nfiles; l++) selected[l] = 0;
////			
////						float radius = 3 *cbrt(2*43.0071*_m200[k][i] / (200*0.25*10000));
////	//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
////						float cube[6];//hx0,hx1,hy0,hy1,hz0,hz1;
////						cube[0] = cm[0]-radius;
////						cube[1] = cm[0]+radius;
////						cube[2] = cm[1]-radius;
////						cube[3] = cm[1]+radius;
////						cube[4] = cm[2]-radius;
////						cube[5] = cm[2]+radius;
////						int i0,i1,j0,j1,k0,k1;
////						i0 = floor( cube[0] / 3000. * HashTabSize );
////						i1 = floor( cube[1] / 3000. * HashTabSize );
////						j0 = floor( cube[2] / 3000. * HashTabSize );
////						j1 = floor( cube[3] / 3000. * HashTabSize );
////						k0 = floor( cube[4] / 3000. * HashTabSize );
////						k1 = floor( cube[5] / 3000. * HashTabSize );
////	//					std::cout << "\tcm: " << k << "\t" << i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << "\t" << radius << std::endl;
////	//					std::cout << "\tcube: " << cube[0] << "\t" << cube[1] << "\t" << cube[2] << "\t" << cube[3] << "\t" << cube[4] << "\t" << cube[5] << std::endl;
////	//					std::cout << "\tcube: " << cube[0]/ 3000. * HashTabSize << "\t" << cube[1]/ 3000. * HashTabSize << "\t" << cube[2]/ 3000. * HashTabSize << "\t" << cube[3]/ 3000. * HashTabSize << "\t" << cube[4] / 3000. * HashTabSize<< "\t" << cube[5] / 3000. * HashTabSize << std::endl;
////	//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
////	//					std::cout << "\thash: " << (-i0)%HashTabSize << "\t" << i0%HashTabSize << "\t" << (-j0)%HashTabSize << "\t" << j0%HashTabSize << "\t" << (-k0)%HashTabSize << "\t" << k0%HashTabSize  << std::endl;
////	
////	//					std::cout << "File " << k << "\tHalo " << i << "\tRadius "<< radius << std::endl;
////						if (load_files(selected,i0,i1+1,j0,j1+1,k0,k1+1));// return 1;
////	//	std::cout << "selected: \t";
////	//	for (int l = 0; l < nfiles; l++) if (selected[l]) std::cout << l << "\t";
////	//	std::cout << std::endl;
////	//						return 0;


////						std::vector<particle> hpart;
////						hpart.resize(0);
////						int ii = 0;

////	//					int l = k;
////						for (int l = 0; l < nfiles; l++)
////						{
////							if (selected[l])
////							{

////								float px,py,pz;
////								hpart.reserve(hpart.size()+(int)(_nparticles[l]/10));
////								if (cube[0] > 0 && cube[1] < 3000 && cube[2] > 0 && cube[3] < 3000 && cube[4] > 0  && cube[5] < 3000) 
////								{
////	//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, cube);
////									read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, cube);
////	 							} else {
////	//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, radius);
////									read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, radius);								
////								}

////								hpart.resize(ii);
////							}

////						}

////	//					 COMPUTE SHAPES
////						halo thalo(haloindex[k]+i,ii,radius,cm[0],cm[1],cm[2],hpart);
////						thalo.reshape();
////						fso << thalo << std::endl;
////						int it = get_shape(thalo);
////						feo << thalo << '\t' << it << std::endl;

////	//						fso << haloindex[k]+i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << std::endl;



////	//					COMPUTE DENSITY PROFILES
////	//					float rcri = cbrt( _mcri[k][i] / cosmo::delta_c/ cosmo::rho_back / (4.*M_PI/3.)  );
////	//					halo thalo(haloindex[k]+i,ii,rcri*rcri,cm[0],cm[1],cm[2],hpart);

////	//{
////	//					halo thalo(haloindex[k]+i,ii,_rvir[_id[haloindex[k]+i]],cm[0],cm[1],cm[2],hpart);


////	//	float rcri = thalo.rcri();
////	//	fso << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << std::endl;
////	//	float density[nbin];
////	//	float rmean[nbin];
////	//	for (int jj = 0; jj < nbin; jj++) 
////	//	{
////	//		density[jj] = 0;
////	//		rmean[jj] = 0;
////	//	}
////	//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
////	//	if ( pippo ) 
////	//	{
////	//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
////	//		return 1;
////	//	}
////	//	

////	//	int idpippo = thalo.id();
////	//	fprofs.write((char*)&idpippo,4);
////	//	fprofs.write((char*)rmean,4*nbin);
////	//	fprofs.write((char*)density,4*nbin);
////	//}



////	//					int ih = _id[haloindex[k]+i];
////	//					halo thalo(haloindex[k]+i,ii,_rvir[_id[haloindex[k]+i]],cm[0],cm[1],cm[2],_axis[ih],_evec[ih],hpart);

////	////					get_conc(thalo);

////	//	float rcri = thalo.rcri();
////	//	feo << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << std::endl;
////	//	float density[nbin];
////	//	float rmean[nbin];
////	//	for (int jj = 0; jj < nbin; jj++) 
////	//	{
////	//		density[jj] = 0;
////	//		rmean[jj] = 0;
////	//	}
////	//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
////	//	if ( pippo ) 
////	//	{
////	//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
////	//		return 1;
////	//	}
////	//	int idpippo = thalo.id();
////	//	fprof.write((char*)&idpippo,4);
////	//	fprof.write((char*)rmean,4*nbin);
////	//	fprof.write((char*)density,4*nbin);




//					}
//				}
//			}

//		}
//	}
//	if (ram > maxram) maxram = ram;
////	std::cout << "Transfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;

//			}	
//		}
////	return 0;
//	}	
////	std::cout << "Box : " << box << "\tTransfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;


////	fso.close();
////	feo.close();
////	fprofs.close();
////	fprof.close();

//	return 0;
//}
