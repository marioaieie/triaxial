#ifndef MUSIC_HH
#define MUSIC_HH

#include <iostream>
#include <vector>
#include "halo.hh"
#include "cosmology.hh"
#include "simulation.hh"

struct struct_cell;
struct struct_list;


class music: public simulation{
	public:
		music(const std::string, const int, const int);
		int cell(int k) const;
		int hashtablesize() const { return _HashTabSize;}
		float* load_snap(int);
		int select_files(bool*, int[6]);
		int load_files(int[6]);
		int load_catalouge();
		int select_part(std::vector<particle>&, float*, int, int, float[6]);
		int select_part(std::vector<particle>&, int&, const float[3], const float[6]);
		int select_part(std::vector<particle>&, int&, const float[3], const float);

		int nparticles() const {return _part.size()/3;}
		float* get_part() {return &_part[0];}
		const cosmology cosmo;
		double pmass() const {return _pmass;}
		double boxsize() const {return _boxsize;}
		std::vector<float> cm() const {return _cm; }
		std::vector<float> mass() const {return _mass; }
		int nhalo() const {return _nhalo;}

		
		
		halo get_halo(const int, const float[3], const float);
		halo get_halo(int) {return halo();}

	private:

		const int _snap;
		const std::string _dir;
		const int _nfiles;
		int _nparticles;

		std::vector<float> _part;
		int _nhalo;
		double _pmass;
		double _boxsize;

		int _HashTabSize;
		struct_cell *_Cell;
		struct_list *_List;
		
		std::vector<float> _cm;
		std::vector<float> _mass;


};
#endif

//class music {
//	public:
//		music(const std::string, const int, const int);
//		void load_cell(int,int,int);
//		halo get_halo(int);
//		halo get_halo(int,const float[3],const float);
//		int mass(int i) const { return _mass[i];}
//		float xcm(int i) const { return _cm[3*i];}
//		float ycm(int i) const { return _cm[3*i+1];}
//		float zcm(int i) const { return _cm[3*i+2];}
//		int nhaloes() const { return _nhaloes;}
//	private:
//		int _startfiles;
//		int _nhaloes;
//		std::string _dir;
//		std::vector<int> _files;
//		std::vector<int> _offset;
//		std::vector<float> _cm;
//		std::vector<float> _mass;

//		std::vector<int> _loadedfiles;
//		std::vector<int> _loadedparticles;
//		std::vector<bool> _readedfiles;		
//		std::vector<float*> _partlist;		


//		int HashTabSize;
//		struct_cell *_Cell;
//		struct_list *_List;
//		void load_hash();

//		void load_file(int);
//};
