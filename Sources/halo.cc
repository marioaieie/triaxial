#include <algorithm>
#include <cmath>
#include "halo.hh"

#ifdef __cplusplus
	extern"C" {
#endif
	void calc_eigenvalues_(float* , float* , float* , float* , float* , float* , float*, float*, float*, float* );
#ifdef __cplusplus
	}
#endif

halo::halo(int id, int mass, float radius, float x, float y, float z, std::vector<particle> list) :  _id(id), _mass(mass), _m200(0), _mcri(0), _radius(radius), _x(x), _y(y), _z(z), _mxx(0), _myy(0), _mzz(0), _mxy(0), _mxz(0), _myz(0), _list(list)
{
	_axis[0] = 1;
	_axis[1] = 1;
	_axis[2] = 1;
	_evec[0][0] = 1;
	_evec[1][0] = 0;
	_evec[2][0] = 0;
	_evec[0][1] = 0;
	_evec[1][1] = 1;
	_evec[2][1] = 0;
	_evec[0][2] = 0;
	_evec[1][2] = 0;
	_evec[2][2] = 1;

}

halo::halo() : _id(0), _mass(0), _radius(0), _x(0), _y(0), _z(0), _mxx(0), _myy(0), _mzz(0), _mxy(0), _mxz(0), _myz(0), _list(0)
{
	_axis[0] = 1;
	_axis[1] = 1;
	_axis[2] = 1;
	_evec[0][0] = 1;
	_evec[1][0] = 0;
	_evec[2][0] = 0;
	_evec[0][1] = 0;
	_evec[1][1] = 1;
	_evec[2][1] = 0;
	_evec[0][2] = 0;
	_evec[1][2] = 0;
	_evec[2][2] = 1;

}

halo::halo(int id, int mass, float radius, float x, float y, float z, float axis[3], float evec[3][3], std::vector<particle> list) : _id(id), _mass(mass), _m200(0), _mcri(0), _radius(radius), _x(x), _y(y), _z(z), _mxx(0), _myy(0), _mzz(0), _mxy(0), _mxz(0), _myz(0), _list(list)
{
	_axis[0] = axis[0];
	_axis[1] = axis[1];
	_axis[2] = axis[2];
	_evec[0][0] = evec[0][0];
	_evec[1][0] = evec[1][0];
	_evec[2][0] = evec[2][0];
	_evec[0][1] = evec[0][1];
	_evec[1][1] = evec[1][1];
	_evec[2][1] = evec[2][1];
	_evec[0][2] = evec[0][2];
	_evec[1][2] = evec[1][2];
	_evec[2][2] = evec[2][2];

	float da, db, dc, a, b;
	a = _axis[0]/_axis[2];
	b = _axis[0]/_axis[1];
	for (int i = 0; i < _list.size(); i++)
	{
		dc = _evec[0][0]*_list[i].x() + _evec[1][0]*_list[i].y() + _evec[2][0]*_list[i].z();
		db = _evec[0][1]*_list[i].x() + _evec[1][1]*_list[i].y() + _evec[2][1]*_list[i].z();
		da = _evec[0][2]*_list[i].x() + _evec[1][2]*_list[i].y() + _evec[2][2]*_list[i].z();
		_list[i] = particle(_list[i].x(),_list[i].y(),_list[i].z(),dc*dc+db*db*b+da*da*a);
	}
}

//halo::halo(halo const &chalo) : _id(chalo._id), _mass(chalo._mass), _x(chalo.x()), _y(chalo.y()), _z(chalo.z()), _mxx(chalo._mxx), _myy(chalo._myy), _mzz(chalo._mzz), _mxy(chalo._mxy), _mxz(chalo._mxz), _myz(chalo._myz)
//{
//	_list = (new std::vector<particle>(chalo._list,chalo._list+_mass))->begin();
//	_axis[0] = chalo._axis[0];
//	_axis[1] = chalo._axis[1];
//	_axis[2] = chalo._axis[2];
//	_evec[0][0] = chalo._evec[0][0];
//	_evec[1][0] = chalo._evec[1][0];
//	_evec[2][0] = chalo._evec[2][0];
//	_evec[0][1] = chalo._evec[0][1];
//	_evec[1][1] = chalo._evec[1][1];
//	_evec[2][1] = chalo._evec[2][1];
//	_evec[0][2] = chalo._evec[0][2];
//	_evec[1][2] = chalo._evec[1][2];
//	_evec[2][2] = chalo._evec[2][2];

////	std::cout << "copy-constructor" << std::endl;
//}

//halo::~halo()
//{
//	delete &(*_list);
////	std::cout << "DELETED" << std::endl;
//}
//// Assignments modify & return lvalue.
//// operator= can only be a member function:
//halo& halo::operator=(const halo& right) {
//	if(this == &right) return *this;	// Handle self-assignment:
//	_list = (new std::vector<particle>(right._list,right._list+right._mass))->begin();
//	_id = right._id;
//	_mass = right._mass;
//	_x = right._x;
//	_y = right._y;
//	_z = right._z;
//	_mxx = right._mxx;
//	_myy = right._myy;
//	_mzz = right._mzz;
//	_mxy = right._mxy;
//	_mxz = right._mxz;
//	_myz = right._myz;
//	_axis[0] = right._axis[0];
//	_axis[1] = right._axis[1];
//	_axis[2] = right._axis[2];
//	_evec[0][0] = right._evec[0][0];
//	_evec[1][0] = right._evec[1][0];
//	_evec[2][0] = right._evec[2][0];
//	_evec[0][1] = right._evec[0][1];
//	_evec[1][1] = right._evec[1][1];
//	_evec[2][1] = right._evec[2][1];
//	_evec[0][2] = right._evec[0][2];
//	_evec[1][2] = right._evec[1][2];
//	_evec[2][2] = right._evec[2][2];
//	
//	return *this;
//}

//		Gives back an halo selecting particles inside ellipsoid (a^2,b^2,c^2)
//		Always gives a SUB-sample of the halo ( new mass <= old mass)
int halo::reshape(float r, float ncm[3]) const
{
	float dx, dy, dz, r2;//, da, db, dc, rell, a, b, c;
	double xcm, ycm, zcm;
	int j = 0;

//	a = 1./eigen[2];
//	b = 1./eigen[1];
//	c = 1./eigen[0];
	r = r*r;
	xcm = 0;
	ycm = 0;
	zcm = 0;

	for (int i = 0; i < _mass; i++)
	{
		float x1,y1,z1;
		x1=_list[i].x();
		y1=_list[i].y();
		z1=_list[i].z();
		
		dx = x1-ncm[0];
		dy = y1-ncm[1];
		dz = z1-ncm[2];

//		dc = _evec[0][0]*dx + _evec[1][0]*dy + _evec[2][0]*dz;
//		db = _evec[0][1]*dx + _evec[1][1]*dy + _evec[2][1]*dz;
//		da = _evec[0][2]*dx + _evec[1][2]*dy + _evec[2][2]*dz;

//		rell = c*dc*dc + b*db*db + a*da*da;
		r2 = dx*dx + dy*dy + dz*dz;
		if ( r2 < r )
		{
			xcm += x1;
			ycm += y1;
			zcm += z1;		
			j++;
		}
	}

	ncm[0]=xcm/j;
	ncm[1]=ycm/j;
	ncm[2]=zcm/j;
//	std::cout << j << "\t" << ncm[0] << "\t" << ncm[1]  << "\t"<< ncm[2]  << std::endl;
	
	return j;
	
}


int halo::reshape(float delta) 
{
//	std::vector<particle> list = _list;


	std::sort(_list.begin(),_list.end());

//	float delta_v;
//	if (ndelta < 0) delta_v = -ndelta;
//	else {
//	//	rhob = 6720./3000.;
//	//	rhob = rhob * rhob * rhob;
//	//	delta_v = 376.8615168784338;
//		delta_v = 89.934955076540/0.25004925418253 *ndelta; // snap 63
//	//	delta_v = 204.85532255399144 *ndelta; // snap 41
//	//	delta_v = 200.;
//	//	delta_v = _radius**3 * sqrt(_axis[1]*_axis[2])/_axis[0];
//	}
	delta *= 4./3.*M_PI *sqrt(_axis[1]*_axis[2])/_axis[0];

//	float delta_cri = cosmo::delta_c *4./3.*M_PI *cosmo::rhob_part * sqrt(_axis[1]*_axis[2])/_axis[0];


//	r200 = ( m200 / DeltaMean200/ rho_back / (4.*np.pi/3)  )**(1./3)

//	delta_v = (18*M_PI*M_PI + 82*(0.25-1) - 39*(0.25-1)*(0.25-1) )* 3*73*73/(8*M_PI*);
//	float delta_c = 89.934955076540;
//	float mpart = 8.456*0.73;
//	float delta_c = 97.141276350345;
//	float mpart = 1.73;
//	delta_v = delta_c*10000. / (mpart*2.*4.302) * sqrt(_axis[1]*_axis[2])/_axis[0];
	double rx, ry, rz, r3;
	double mxx, myy, mzz, mxy, mxz, myz;
	mxx = myy = mzz = mxy = mxz = myz = 0;
	double xcm, ycm, zcm;
	xcm = 0;
	ycm = 0;
	zcm = 0;

	_mass = 0;
	int ii =0;
	do
	{
		rx = _list[ii].x();
		ry = _list[ii].y();
		rz = _list[ii].z();
		r3 = _list[ii].r() * sqrt(_list[ii].r());
//		if (r3>r200_3) return 1;
		xcm += _list[ii].mass()*_list[ii].x();
		ycm += _list[ii].mass()*_list[ii].y();
		zcm += _list[ii].mass()*_list[ii].z();
		mxx += _list[ii].mass()*rx*rx;
		myy += _list[ii].mass()*ry*ry;
		mzz += _list[ii].mass()*rz*rz;
		mxy += _list[ii].mass()*rx*ry;
		mxz += _list[ii].mass()*rx*rz;
		myz += _list[ii].mass()*ry*rz;
		_mass += _list[ii].mass();
		ii++;
	} while ( _mass / r3 > delta && ii < _list.size() );

	_mass -= _list[ii-1].mass();
	_radius = _list[ii-1].r();
	_mxx = mxx / _mass;
	_myy = myy / _mass;
	_mzz = mzz / _mass;
	_mxy = mxy / _mass;
	_mxz = mxz / _mass;
	_myz = myz / _mass;
	
	_x = xcm / _mass;
	_y = ycm / _mass;
	_z = zcm / _mass;
	
//	std::cout << _list[1].x()  << "\t" << _list[1].y()  << "\t" << _list[1].z()  << "\t" << _list[1].r() << std::endl;
//	std::cout << _list.size()  << "\t" << ii  << std::endl;

//	ii++;

//	delta_v = 200.;
//	delta_v = (delta_v )*4./3.*M_PI *cosmo::rhob_part * sqrt(_axis[1]*_axis[2])/_axis[0];
//	do
//	{
//		rx = _list[ii].x();
//		ry = _list[ii].y();
//		rz = _list[ii].z();
//		r3 = _list[ii].r() * sqrt(_list[ii].r());
//		ii++;
//	} while ( ii / r3 > delta_v && ii < _list.size() );
//	_m200 = --ii;

	mtensor();
	
	float da, db, dc, a, b;
	a = _axis[0]/_axis[2];
	b = _axis[0]/_axis[1];
//	std::cout << 1/sqrt(a) << "\t" << 1/sqrt(b) << std::endl<< std::endl;
	//c = 1./sqrt(_axis[0]);
	for (int i = 0; i < _list.size(); i++)
	{
		dc = _evec[0][0]*_list[i].x() + _evec[1][0]*_list[i].y() + _evec[2][0]*_list[i].z();
		db = _evec[0][1]*_list[i].x() + _evec[1][1]*_list[i].y() + _evec[2][1]*_list[i].z();
		da = _evec[0][2]*_list[i].x() + _evec[1][2]*_list[i].y() + _evec[2][2]*_list[i].z();
//		dc = _list[i].x();
//		db = _list[i].y();
//		da = _list[i].z();
		_list[i] = particle(_list[i].x(),_list[i].y(),_list[i].z(),dc*dc+db*db*b+da*da*a,_list[i].mass());
	}
//	std::sort(_list.begin(),_list.end());
	
	return 0;
	
}



void halo::mtensor()
{
	//if (_mxx == 0) compute_mtensor(); 

	float mxx, myy, mzz, mxy, mxz, myz;
	float d2[3],ev0[3],ev1[3],ev2[3];
	mxx = _mxx;
	myy = _myy;
	mzz = _mzz;
	mxy = _mxy;
	mxz = _mxz;
	myz = _myz;

	calc_eigenvalues_(&mxx, &myy, &mzz, &mxy, &mxz, &myz, d2, ev0, ev1, ev2);
	
//	cout << "c++" << endl;
//	cout << d2[0] << "\t" << ev0[0] << "\t" << ev0[1] << "\t" << ev0[2] << endl;
//	cout << d2[1] << "\t" << ev1[0] << "\t" << ev1[1] << "\t" << ev1[2] << endl;
//	cout << d2[2] << "\t" << ev2[0] << "\t" << ev2[1] << "\t" << ev2[2] << endl << endl;

	if ( d2[0] > d2[1] ) 
	{
		_axis[0] = d2[0];
		_evec[0][0] = ev0[0];
		_evec[1][0] = ev0[1];
		_evec[2][0] = ev0[2];
		
		_axis[1] = d2[1];
		_evec[0][1] = ev1[0];
		_evec[1][1] = ev1[1];
		_evec[2][1] = ev1[2];
	} else {
		_axis[0] = d2[1];
		_evec[0][0] = ev1[0];
		_evec[1][0] = ev1[1];
		_evec[2][0] = ev1[2];
		
		_axis[1] = d2[0];
		_evec[0][1] = ev0[0];
		_evec[1][1] = ev0[1];
		_evec[2][1] = ev0[2];
	}

	if ( _axis[1] > d2[2] ) 
	{
		_axis[2] = d2[2];
		_evec[0][2] = ev2[0];
		_evec[1][2] = ev2[1];
		_evec[2][2] = ev2[2];
		
	} else {
		_axis[2] = _axis[1];
		_evec[0][2] = _evec[0][1];
		_evec[1][2] = _evec[1][1];
		_evec[2][2] = _evec[2][1];
		
		_axis[1] = d2[2];
		_evec[0][1] = ev2[0];
		_evec[1][1] = ev2[1];
		_evec[2][1] = ev2[2];
	}

	if ( _axis[0] < _axis[1] ) 
	{
		float temp,temp0,temp1,temp2;
		temp = _axis[0];
		temp0 = _evec[0][0];
		temp1 = _evec[1][0];
		temp2 = _evec[2][0];
		
		_axis[0] = _axis[1];
		_evec[0][0] = _evec[0][1];
		_evec[1][0] = _evec[1][1];
		_evec[2][0] = _evec[2][1];
		
		_axis[1] = temp;
		_evec[0][1] = temp0;
		_evec[1][1] = temp1;
		_evec[2][1] = temp2;
	}
	
//	cout << _axis[0] << "\t" << _axis[1] << "\t" << _axis[2]<< endl<< endl;
//	cout << _axis[0] << "\t" << _evec[0][0] << "\t" << _evec[1][0] << "\t" << _evec[2][0] << endl;
//	cout << _axis[1] << "\t" << _evec[0][1] << "\t" << _evec[1][1] << "\t" << _evec[2][1] << endl;
//	cout << _axis[2] << "\t" << _evec[0][2] << "\t" << _evec[1][2] << "\t" << _evec[2][2] << endl<< endl;

}
	
void halo::eigenvalues(float eigen[3]) const
{
	eigen[0] = _axis[0];
	eigen[1] = _axis[1];
	eigen[2] = _axis[2];
	
	return;
}

void halo::eigenvectors(float eigen[3][3]) const
{
	eigen[0][0] = _evec[0][0];
	eigen[1][0] = _evec[1][0];
	eigen[2][0] = _evec[2][0];
	eigen[0][1] = _evec[0][1];
	eigen[1][1] = _evec[1][1];
	eigen[2][1] = _evec[2][1];
	eigen[0][2] = _evec[0][2];
	eigen[1][2] = _evec[1][2];
	eigen[2][2] = _evec[2][2];
	
	return;
}

void halo::compute_mtensor()
{
	float rx, ry, rz;
	double mxx, myy, mzz, mxy, mxz, myz;
	mxx = myy = mzz = mxy = mxz = myz = 0;
	
	for (int i = 0; i < _list.size(); i++)
	{
		rx = _list[i].x();
		ry = _list[i].y();
		rz = _list[i].z();

		mxx += _list[i].mass()*rx*rx;
		myy += _list[i].mass()*ry*ry;
		mzz += _list[i].mass()*rz*rz;
		mxy += _list[i].mass()*rx*ry;
		mxz += _list[i].mass()*rx*rz;
		myz += _list[i].mass()*ry*rz;
	}	
	
	_mxx = mxx / _mass;
	_myy = myy / _mass;
	_mzz = mzz / _mass;
	_mxy = mxy / _mass;
	_mxz = mxz / _mass;
	_myz = myz / _mass;
		
	return;
}

//float halo::rcri()
//{
//	std::sort(_list.begin(),_list.end());

//	float delta_v = (cosmo.delta_c)*4./3.*M_PI *cosmo.rhob_part * sqrt(_axis[1]*_axis[2])/_axis[0];

//	double rx, ry, rz, r3;

//	int ii =0;
//	do
//	{
//		rx = _list[ii].x();
//		ry = _list[ii].y();
//		rz = _list[ii].z();
//		r3 = _list[ii].r() * sqrt(_list[ii].r());
//		ii++;
//	} while ( ii / r3 > delta_v && ii < _list.size() );

//	_mcri = ii-1;
//	return _list[_mcri].r();
//}
	
int halo::denprofile(float lrmin, float lrmax, int nbin, float* radius, float* density)
{
	float rcri = _list[_mcri].r();
	float rmin = pow(10,lrmin)*sqrt(rcri);
	float rmax = pow(10,lrmax)*sqrt(rcri);

	double rmean[nbin];
	for (int i = 0; i < nbin; i++) rmean[i] = 0;

	for (int i = 0; i < _list.size(); i++)
	{
		if (sqrt(_list[i].r()) > rmin && sqrt(_list[i].r()) < rmax)
		{
			int ibin = (int)(( log10(sqrt(_list[i].r()/rcri)) - lrmin ) / ( lrmax - lrmin ) * nbin);
			if ( ibin > 31 && ibin < 0 ) 
			{
			std::cout << "PIPPO!!! ";
			return ibin;
			}
			density[ibin]++;
			rmean[ibin] = rmean[ibin] + sqrt(_list[i].r());
		}
	}
	
	for (int i = 0; i < nbin; i++)
	{
		if (density[i] != 0)
		{
			radius[i] = rmean[i] / density[i];
			density[i] = density[i] / (4./3. * M_PI * radius[i] * radius[i] * radius[i]);
		}
	}
	return 0;


}
	
	
//void halo::cm(int mass, float ncm[3]) const
//{
//	double xcm, ycm, zcm;
//	xcm = 0;
//	ycm = 0;
//	zcm = 0;

//	for (int i = 0; i < mass; i++)
//	{
//		xcm += _list[i].x();
//		ycm += _list[i].y();
//		zcm += _list[i].z();
//	}

//	ncm[0]=xcm/mass;
//	ncm[1]=ycm/mass;
//	ncm[2]=zcm/mass;
//	
//	return;
//}

//template<typename T>
//std::ostream& binary_write(std::ostream& stream, const T& value){
//    return stream.write(reinterpret_cast<const char*>(&value), sizeof(T));
//}

//void halo::write(std::ostream& file)
//{
////	fso[omp_get_thread_num()].write((char*)&tbin,4);
//	int pippo = 0;
//	file.write();
//	stream.write(reinterpret_cast<const char*>(&pippo), sizeof(pippo));
////	filep.write((char*)&_id,4);
////	filep.write((char*)&_mass,4);
////	filep.write((char*)&_radius,4);
////	float tcm[3] = {_x,_y,_z};
////	file.write((char*)tcm,4*3);
////	file.write((char*)_axis,4*3);
////	file.write((char*)_evec,4*9);
//	return;
//}

std::ostream& operator<< (std::ostream& os, const halo& self )
{
//	os << self._id << "\t" << self._mass << "\t" << self._mcri << "\t" << self._m200 << "\t" << std::scientific << self._radius << "\t"
	os << self._id << "\t" << std::scientific << self._mass << "\t" << self._radius << "\t"
	<< self._x << "\t" << self._y << "\t" << self._z << "\t"
	<< self._axis[0] << "\t" << self._axis[1] << "\t" << self._axis[2] << "\t"
	<< self._evec[0][0] << "\t" << self._evec[1][0] << "\t" << self._evec[2][0] << "\t"
	<< self._evec[0][1] << "\t" << self._evec[1][1] << "\t" << self._evec[2][1] << "\t"
	<< self._evec[0][2] << "\t" << self._evec[1][2] << "\t" << self._evec[2][2];
	
	return os;
}

//std::istream& operator<< (std::istream& os, const halo& self )
//{
//	int id, _mass, _m200;
//	float _x, _y, _z, _radius;
//	float _axis[3];
//	float _evec[3][3];

//	os >> id >> _mass >> _m200 >> _radius >> _x >> _y >> _z
//	>> _axis[0] >> _axis[1] >> _axis[2]
//	>> _evec[0][0] >> _evec[1][0] >> _evec[2][0]
//	>> _evec[0][1] >> _evec[1][1] >> _evec[2][1]
//	>> _evec[0][2] >> _evec[1][2] >> _evec[2][2];
//	
//	halo(int, int, float, float, float, float, float [3], float[3][3])
//	
//	return os;
//}

//halo::halo(int, int, float, float, float, float, float [3], float[3][3]);

		
float halo::dum()
{
		double xcm = 0;
		for (int i = 0; i < _mass; i++)
		{
			xcm += _list[i].x();
		}
		return xcm/_mass;
//		_list[i].coord();
//		return 0.;
}
