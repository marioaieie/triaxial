#ifndef simulation_HH
#define simulation_HH

#include "halo.hh"

class simulation {
	public:
	simulation(const std::string, const int, const int) { std::cout << "Opening simulations files..."; }
	virtual halo get_halo(int) = 0;
	private:
//	int _nhalo;
//	std::vector<float> _mass;
//	std::vector<float> _cm;
//	std::string _dir;
};
#endif
