#include <iostream>
#include <fstream>
#include <sstream>

float *partlist;


void load_snap(int k)
{
	std::ostringstream s2;
	s2 << "/home/data/Mxxl/Snapshot_063/snap_mxxl." << k;
	std::ifstream fp(s2.str().c_str(),std::ios::binary|std::ios::in);
	if (fp.is_open())
	{
		std::cout << "Opened file:\t" << s2.str() << std::endl;

		//	reading header
		int nread;
		fp.read((char*)&nread,4);

		partlist = new float[3*nread];
		fp.read((char*)partlist,12*nread);
		fp.close();

		std::cout << "Particles readed! " << partlist[0] << std::endl;

	} else {
		std::cout << "Could not open file " << s2.str() << std::endl;
	}

}


int main(int argc, char* argv[])
{
	{
		load_snap(10);
	}
}
