#include <iostream>
#include <fstream>
#include <iomanip> // setprecision()
#include <limits>
#include <cmath>
#include <sstream>
#include <vector>
#include <omp.h>
#include <cstdlib> // random
//#include "particle.hh"
//#include "halo.hh"
#include "cosmo.hh"
#include <uns.h>
//#include "inc/uns/uns.h"
#include "mxxl.hh"


template<typename T>
std::ostream& binary_write(std::ostream& stream, const T& value){
    return stream.write(reinterpret_cast<const char*>(&value), sizeof(T));
}
void writeonfile(std::ofstream& fileout,halo& thalo)
{				
	float tcm[3] = {thalo.x(),thalo.y(),thalo.z()};
	float eigval[3],eigvec[3][3];
	thalo.eigenvectors(eigvec);
	thalo.eigenvalues(eigval);
	binary_write(fileout,thalo.id());
	binary_write(fileout,thalo.mass());
	binary_write(fileout,thalo.radius());
	binary_write(fileout,tcm);
	binary_write(fileout,eigval);
	binary_write(fileout,eigvec);
//	binary_write(fileout,1);
}


int get_shape(halo &thalo, float ndelta)
{

	float eig_halo[3];
	float s,q,s1,q1;
	thalo.eigenvalues(eig_halo);
	q1 = sqrt(eig_halo[1]/eig_halo[0]);
	s1 = sqrt(eig_halo[2]/eig_halo[0]);

	int it=0;
	do 
	{
		s = s1;
		q = q1;
//		f11 << it << '\t' << s << '\t' << q << std::endl;

		thalo.reshape(ndelta);

		thalo.eigenvalues(eig_halo);
		q1 = sqrt(eig_halo[1]/eig_halo[0]);
		s1 = sqrt(eig_halo[2]/eig_halo[0]);

		it++;
		if (thalo.mass() < 1000) return -it;

//		} while ( ( (s1-s)*(s1-s)/(s*s) > 0.005 || (q1-q)*(q1-q)/(q*q) > 0.005 ) && it<100);
	} while ( ( std::abs(s1-s)/s > 0.005 || std::abs(q1-q)/q > 0.005 ) && it<100);
//		} while ((std::abs( thalo.rpart(thalo.mass())-rell) /rell) > 0.0001 && it<100);

		return it;
}



int main(int argc, char* argv[])
{
   
	int box,basex,basey,basez;
	box = 10;
	basex = basey = basez = 0;
	switch (argc-1 )
	{
	case 0:
		break;
	case 2:
//		Quite useless, but for testing...
		box = std::atoi(argv[1]);
		basex = basey = basez = std::atoi(argv[2]);
		break;
	case 4:
		box = std::atoi(argv[1]);
		basex = std::atoi(argv[2]);
		basey = std::atoi(argv[3]);
		basez = std::atoi(argv[4]);
		break;
	default:
		std::cout << "Incorrect number of arguments (" << argc << "). Needed 0,2 or 4" << std::endl;
		return 0;
	}

//	const std::string dir = "/data/mbonamigo/Mxxl/";
	const std::string dir = "/home/data/Mxxl/";
	const int nfiles = 3072;
	const int snap = 63;

	int minibox[] = {basex,basex+box,basey,basey+box,basez,basez+box};


	mxxl sim(dir,snap,nfiles);

	sim.load_files(minibox);

	float* part = sim.getPart();

	int pippo = 0;
	int npart = sim.nparticles();
	int N=10;
	int idmax = ceil(npart/float(N));
	int *id;
	float *pos;
	std::cout << idmax << "\t" << 3*idmax << "\t" << npart-(N-1)*idmax << std::endl;
	for(int i=0; i<N; i++)
	{
		int jmax = idmax;
		if (i==N-1) jmax = npart-(N-1)*idmax;
		id = new int[idmax];
		pos = new float[3*idmax];
		if (jmax>0)
		{
			for(int j=0; j<jmax; j++)
			{
				int idj = i*idmax + j;
				id[j] = idj;
//				std::cout << id[j] << "\t" << 3*j+0 << " " << 3*j+1 << " " << 3*j+2 << "\t" << 3*id[j]+0 << " " << 3*id[j]+1 << " " << 3*id[j]+2 << std::endl;
				pos[3*j+0] = part[3*idj+0];
				pos[3*j+1] = part[3*idj+1];
				pos[3*j+2] = part[3*idj+2];
			}
			std::cout << "----------------" << std::endl;
			std::ostringstream s2;
			s2 << "miniXXL." << i;
			uns::CunsOut * uns = new uns::CunsOut(s2.str(),"gadget2",0);
			bool ok;
			ok = uns->snapshot->setData("dm","id",jmax,id,true);
			ok = uns->snapshot->setData("dm","pos",jmax,pos,true);
			std::cout << "data "<< i << ": " << ok << std::endl;
			ok = uns->snapshot->save();
			std::cout << "Saved "<< i << ": " << ok << std::endl;
			pippo += jmax;
		}
	}
	std::cout<<pippo << std::endl;




// 	ALL HALOES
//	std::ofstream fso,feo;
//	std::ostringstream seo,sso;
//	seo << "halo_EO_" << startfiles << "-" << startfiles+nfiles-1 << ".dat";
//	feo.open(seo.str().c_str(),std::ios::out);
//	sso << "halo_SO_" << startfiles << "-" << startfiles+nfiles-1 << ".dat";
//	fso.open(sso.str().c_str(),std::ios::out);
//	for (int j = 0; j < sim.nhaloes(); j++)
//	{
		//		int idum;
		//		float fdum,xcm,ycm,zcm,xcmf,ycmf,zcmf;
		//		f_centre >> idum >> idum >> xcmf >> ycmf >> zcmf >> xcm >> ycm >> zcm;
		//	
		////		std::cout << xcm << "\t" << ycm << "\t" << zcm << std::endl;
		////		std::cout << "Halo: " << j << std::endl;
		//	
		//		float cm[3];
		//		cm[0] = xcm + xcmf;
		//		cm[1] = ycm + ycmf;
		//		cm[2] = zcm + zcmf;






//		halo thalo = sim.get_halo(j);

//					 COMPUTE SHAPES
//		float dens[10];
//		for (int nn = 0; nn < 7; nn++) dens[nn] = pow(2.,nn);
//		dens[7] = -200./cosmo::omegaz;
//		dens[8] = -500./cosmo::omegaz;
//		dens[9] = -2500./cosmo::omegaz;

//		for (int nn = 0; nn < 10; nn++){
//			halo thalo(_todo[i],ii,radius,cm[0],cm[1],cm[2],hpart);
//			thalo.reshape(dens[nn]);
//			writeonfile(fso[omp_get_thread_num()],thalo);
//			binary_write(fso[omp_get_thread_num()],0);
//			int it = 0;
//			if (thalo.mass() >= 1000) it = get_shape(thalo,dens[nn]);
//			writeonfile(feo[omp_get_thread_num()],thalo);
//			binary_write(feo[omp_get_thread_num()],it);
//		}

//	}	
//	std::cout << "Box : " << box << "\tTransfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;


//	fso.close();
//	feo.close();
//	fprofs.close();
//	fprof.close();

	return 0;
}	
	
	
	

//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (_inside[k])
//		{
//			// Do your stuff...
////			std::cout << "I'm doing my stuff... " << _nhaloes[k] << std::endl;
////		if (load_head(k)) return 1;

//			for (int i = 0; i < _nhaloes[k]; i++)
//			{

//				if (_todo[haloindex[k]+i])
//				{
//					if (_cm[k][3*i] <= x1 && _cm[k][3*i] > x0 && _cm[k][3*i+1] <= y1 && _cm[k][3*i+1] > y0 && _cm[k][3*i+2] <= z1 && _cm[k][3*i+2] > z0 )
//					{
//						float radius = 3 *cbrt(2*43.0071*_m200[k][i] / (200*0.25*10000));
////						if (_id[haloindex[k]+i] == -1)
////						{
////							std::cout << "mmm... sth smells here... " << haloindex[k]+i << std::endl;
////							return 1;
////						}
//	//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
//					
//						int i0,i1,j0,j1,k0,k1;
//						i0 = floor( (_cm[k][3*i]-radius) / 3000. * HashTabSize );
//						i1 = floor( (_cm[k][3*i]+radius) / 3000. * HashTabSize );
//						j0 = floor( (_cm[k][3*i+1]-radius) / 3000. * HashTabSize );
//						j1 = floor( (_cm[k][3*i+1]+radius) / 3000. * HashTabSize );
//						k0 = floor( (_cm[k][3*i+2]-radius) / 3000. * HashTabSize );
//						k1 = floor( (_cm[k][3*i+2]+radius) / 3000. * HashTabSize );
//	
//	//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
//						if (load_files(needed,i0,i1+1,j0,j1+1,k0,k1+1)) return 1;
//					}
//				}
//			}
//		}
//	}

////	std::cout << "Inside: \t";
////	for (int k = 0; k < nfiles; k++) if (_inside[k]) std::cout << k << "\t";
////	std::cout << std::endl;

////	std::cout << "Needed: \t";
//	ram = 0;
//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (needed[k])
//		{
////			std::cout << k << "\t";
//			ram++;
//		}
//	}
////	std::cout << std::endl;
//	if (ram>30)
//	{
//		std::cout << "Too much RAM!!!";
//		return 1;
//	}

////	std::cout << "Partlist: \t";
////	for (int k = 0; k < nfiles; k++) if (_partlist[k]) std::cout << k << "\t";
////	std::cout << std::endl;

//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (_partlist[k] && !needed[k] && !_inside[k])
//		{
////			std::cout << "Unloading file " << k << std::endl;
////			delete _partlist[k];
//			_partlist[k] = 0;
//		}
//	}
//	for (int k = 0; k < nfiles; k++) 
//	{
//		if (!_partlist[k] && needed[k])			
//		{
//			// Load files
////			std::cout << "Loading file " << k << std::endl;
//			count++;
////			if (load_snap(k)) return 1;
////			_partlist[k] = 1;
//		}
//	}

////	std::cout << "Loading, DONE!" << std::endl;

//	for (int k = 0; k < nfiles; k++) 
//	{
////		if (files[l]) std::cout << l*files[l] << "\t";
//		if (_inside[k])
//		{
//			// Do your stuff...
////			#pragma omp parallel for default(shared) reduction(+:numhalo)
//			for (int i = 0; i < _nhaloes[k]; i++)
//			{
//				if (_todo[haloindex[k]+i])
//				{
//					float cm[3];
//					cm[0] = _cm[k][3*i+0];
//					cm[1] = _cm[k][3*i+1];
//					cm[2] = _cm[k][3*i+2];
//				
//					if (cm[0] <= x1 && cm[0] > x0 && cm[1] <= y1 && cm[1] > y0 && cm[2] <= z1 && cm[2] > z0 )
//					{
//				
//							numhalo++;
//							std::cout << haloindex[k]+i << std::endl;
//						
//						
//	//		\/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/    \/

////						bool selected[nfiles];
////						for (int l = 0; l < nfiles; l++) selected[l] = 0;
////			
////						float radius = 3 *cbrt(2*43.0071*_m200[k][i] / (200*0.25*10000));
////	//					float radius = sqrt(_rvir[_id[haloindex[k]+i]]);
////						float cube[6];//hx0,hx1,hy0,hy1,hz0,hz1;
////						cube[0] = cm[0]-radius;
////						cube[1] = cm[0]+radius;
////						cube[2] = cm[1]-radius;
////						cube[3] = cm[1]+radius;
////						cube[4] = cm[2]-radius;
////						cube[5] = cm[2]+radius;
////						int i0,i1,j0,j1,k0,k1;
////						i0 = floor( cube[0] / 3000. * HashTabSize );
////						i1 = floor( cube[1] / 3000. * HashTabSize );
////						j0 = floor( cube[2] / 3000. * HashTabSize );
////						j1 = floor( cube[3] / 3000. * HashTabSize );
////						k0 = floor( cube[4] / 3000. * HashTabSize );
////						k1 = floor( cube[5] / 3000. * HashTabSize );
////	//					std::cout << "\tcm: " << k << "\t" << i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << "\t" << radius << std::endl;
////	//					std::cout << "\tcube: " << cube[0] << "\t" << cube[1] << "\t" << cube[2] << "\t" << cube[3] << "\t" << cube[4] << "\t" << cube[5] << std::endl;
////	//					std::cout << "\tcube: " << cube[0]/ 3000. * HashTabSize << "\t" << cube[1]/ 3000. * HashTabSize << "\t" << cube[2]/ 3000. * HashTabSize << "\t" << cube[3]/ 3000. * HashTabSize << "\t" << cube[4] / 3000. * HashTabSize<< "\t" << cube[5] / 3000. * HashTabSize << std::endl;
////	//					std::cout << "\thash: " << i0 << "\t" << i1 << "\t" << j0 << "\t" << j1 << "\t" << k0 << "\t" << k1  << std::endl;
////	//					std::cout << "\thash: " << (-i0)%HashTabSize << "\t" << i0%HashTabSize << "\t" << (-j0)%HashTabSize << "\t" << j0%HashTabSize << "\t" << (-k0)%HashTabSize << "\t" << k0%HashTabSize  << std::endl;
////	
////	//					std::cout << "File " << k << "\tHalo " << i << "\tRadius "<< radius << std::endl;
////						if (load_files(selected,i0,i1+1,j0,j1+1,k0,k1+1));// return 1;
////	//	std::cout << "selected: \t";
////	//	for (int l = 0; l < nfiles; l++) if (selected[l]) std::cout << l << "\t";
////	//	std::cout << std::endl;
////	//						return 0;


////						std::vector<particle> hpart;
////						hpart.resize(0);
////						int ii = 0;

////	//					int l = k;
////						for (int l = 0; l < nfiles; l++)
////						{
////							if (selected[l])
////							{

////								float px,py,pz;
////								hpart.reserve(hpart.size()+(int)(_nparticles[l]/10));
////								if (cube[0] > 0 && cube[1] < 3000 && cube[2] > 0 && cube[3] < 3000 && cube[4] > 0  && cube[5] < 3000) 
////								{
////	//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, cube);
////									read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, cube);
////	 							} else {
////	//								read_part(hpart, ii, _partlist[l], _off[l][i], _off[l][i]+_len[l][i], cm, radius);
////									read_part(hpart, ii, _partlist[l], 0, _nparticles[l], cm, radius);								
////								}

////								hpart.resize(ii);
////							}

////						}

////	//					 COMPUTE SHAPES
////						halo thalo(haloindex[k]+i,ii,radius,cm[0],cm[1],cm[2],hpart);
////						thalo.reshape();
////						fso << thalo << std::endl;
////						int it = get_shape(thalo);
////						feo << thalo << '\t' << it << std::endl;

////	//						fso << haloindex[k]+i << "\t" << cm[0] << "\t" << cm[1] << "\t" << cm[2] << std::endl;



////	//					COMPUTE DENSITY PROFILES
////	//					float rcri = cbrt( _mcri[k][i] / cosmo::delta_c/ cosmo::rho_back / (4.*M_PI/3.)  );
////	//					halo thalo(haloindex[k]+i,ii,rcri*rcri,cm[0],cm[1],cm[2],hpart);

////	//{
////	//					halo thalo(haloindex[k]+i,ii,_rvir[_id[haloindex[k]+i]],cm[0],cm[1],cm[2],hpart);


////	//	float rcri = thalo.rcri();
////	//	fso << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << std::endl;
////	//	float density[nbin];
////	//	float rmean[nbin];
////	//	for (int jj = 0; jj < nbin; jj++) 
////	//	{
////	//		density[jj] = 0;
////	//		rmean[jj] = 0;
////	//	}
////	//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
////	//	if ( pippo ) 
////	//	{
////	//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
////	//		return 1;
////	//	}
////	//	

////	//	int idpippo = thalo.id();
////	//	fprofs.write((char*)&idpippo,4);
////	//	fprofs.write((char*)rmean,4*nbin);
////	//	fprofs.write((char*)density,4*nbin);
////	//}



////	//					int ih = _id[haloindex[k]+i];
////	//					halo thalo(haloindex[k]+i,ii,_rvir[_id[haloindex[k]+i]],cm[0],cm[1],cm[2],_axis[ih],_evec[ih],hpart);

////	////					get_conc(thalo);

////	//	float rcri = thalo.rcri();
////	//	feo << thalo.id() << "\t" << thalo.mcri() << "\t" << rcri << std::endl;
////	//	float density[nbin];
////	//	float rmean[nbin];
////	//	for (int jj = 0; jj < nbin; jj++) 
////	//	{
////	//		density[jj] = 0;
////	//		rmean[jj] = 0;
////	//	}
////	//	int pippo = thalo.denprofile(-2.5, 0, 32, rmean, density);
////	//	if ( pippo ) 
////	//	{
////	//		std::cout << thalo.id() << "\tPIPPO2!!! "<< pippo;
////	//		return 1;
////	//	}
////	//	int idpippo = thalo.id();
////	//	fprof.write((char*)&idpippo,4);
////	//	fprof.write((char*)rmean,4*nbin);
////	//	fprof.write((char*)density,4*nbin);




//					}
//				}
//			}

//		}
//	}
//	if (ram > maxram) maxram = ram;
////	std::cout << "Transfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;

//			}	
//		}
////	return 0;
//	}	
////	std::cout << "Box : " << box << "\tTransfered files: "<< count << "\tMax RAM: " << maxram << "\tProcessed haloes: " << numhalo << "\tExited: " << outside << " times"  <<  std::endl;


////	fso.close();
////	feo.close();
////	fprofs.close();
////	fprof.close();

//	return 0;
//}
