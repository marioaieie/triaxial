#ifndef COSMOLOGY_HH
#define COSMOLOGY_HH
#include <cmath>

class cosmology {
	public:
		cosmology(double om=0.3, double z=0., double hubble=100.) : Omega0(om), Hubble(hubble), rho_back(rhob()), omegaz(omz(z)), rho_cri(rhoc(z)), delta_cri(deltac(z)) {}
		double omz(double z) const { return Omega0 * pow(1+z,3) / (Omega0*pow(1+z,3) + 1-Omega0 ); }
		double rhoc(double z) const { return rho_back / omz(z);}
		double deltac(double z) const { return 200. / omz(z);}
		
		static constexpr double G = 43.0071;
		const double Hubble;
		const double Omega0;
		const double rho_back;
		double omegaz; 
		double rho_cri;
		double delta_cri;
	private:
	double rhob() const { return 3 * Omega0 * Hubble*Hubble / (8 * M_PI * G); }

};
#endif
